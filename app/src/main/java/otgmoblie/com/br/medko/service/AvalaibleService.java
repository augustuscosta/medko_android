package otgmoblie.com.br.medko.service;

import android.content.Context;
import android.content.SharedPreferences;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Thialyson on 26/08/16.
 */

@EBean
public class AvalaibleService {

    @RootContext
    Context context;

    private void saveSharedPreferencesAvailable(boolean valueBoolean, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("PREFERENCES_AVAILABLE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("PREFERENCES_AVAILABLE", valueBoolean);
        editor.apply();
    }

    public boolean getSharedPreferencesAvailable(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("PREFERENCES_AVAILABLE", Context.MODE_PRIVATE);
        return sharedPref.getBoolean("PREFERENCES_AVAILABLE", false);
    }

    public void updateSharedPreferencesAvailable(boolean valueBoolean){
        saveSharedPreferencesAvailable(valueBoolean, context);
    }
}
