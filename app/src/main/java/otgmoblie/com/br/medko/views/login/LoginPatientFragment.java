package otgmoblie.com.br.medko.views.login;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.List;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Session;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.service.DeviceIdService;
import otgmoblie.com.br.medko.service.FacebookService;
import otgmoblie.com.br.medko.service.FacebookServiceCallback;
import otgmoblie.com.br.medko.service.SessionService;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.util.ValidateUtil;
import otgmoblie.com.br.medko.views.home.HomeActivity_;
import otgmoblie.com.br.medko.views.register.RegisterActivity_;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_login_patient)
public class LoginPatientFragment extends Fragment implements FacebookServiceCallback {

    @ViewById(R.id.editTextEmail)
    EditText email;

    @ViewById(R.id.editTextPassword)
    EditText password;

    @ViewById(R.id.tabLayout)
    TabLayout tabLayout;

    @ViewById(R.id.viewPager)
    ViewPager viewPager;

    @Bean
    SessionService sessionService;

    @Bean
    DeviceIdService deviceIdService;

    public FacebookService facebookService;

    private ProgressDialog progress;


    @AfterViews
    void afterViews() {
        facebookService = new FacebookService();
        facebookService.init(getActivity(), this);
    }

    @Click(R.id.buttonLoginPatient)
    void loginButton() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        checkedList.add(ValidateUtil.checkInterval(password, 6, 18, getString(R.string.invalid_password)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            login();
        }
    }


    @Background
    void login() {

        startProgress();

        Session session = new Session();
        session.setEmail(email.getText().toString());
        session.setPassword(password.getText().toString());

        try {
            sessionService.signIn(session);
            deviceIdService.sendDeviceId();
            closeProgress();
            callHomeActivity();

        } catch (ResourceAccessException e) {
            e.printStackTrace();
            showToast(getString(R.string.internet_no_connected));

        } catch (Exception e) {
            e.printStackTrace();
            closeProgress();
            callToastWrongEmailOrPassword();
        }

    }



    @UiThread
    void callToastWrongEmailOrPassword() { showToast(getString(R.string.email_or_password_incorrect)); }

    @Click(R.id.buttonFacebookLogin)
    void facebookButton() {
        facebookService.loginFacebook();
    }

    @Background
    void loginFacebook(User user) {

        startProgress();

        try {
            user.setAccountTypeId(1);

            if (sessionService.signInFacebook(user)) {
                deviceIdService.sendDeviceId();
                closeProgress();
                callHomeActivity();
            }

        } catch (ResourceAccessException e) {
            e.printStackTrace();
            showToast(getString(R.string.internet_no_connected));

        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onFacebookLoginSuccess(User user) {
        loginFacebook(user);
    }

    @Override
    public void onFacebookLoginError(String message) { showToast(getString(R.string.erro_to_acess_facebook)); }

    @Click(R.id.buttonGoToRegisterPatient)
    void goToRegisterPatient() {
        callRegisterActivity(0);
    }

    @Click(R.id.textViewForgotPassword)
    void goToRecoverPassword() {
        callRegisterActivity(2);
    }

    @UiThread
    void callRegisterActivity(Integer key) {
        startActivity(new Intent(getActivity(), RegisterActivity_.class).putExtra(getString(R.string.key), key));
        getActivity().finish();
    }

    @UiThread
    void callHomeActivity() {
        HomeActivity_.intent(getActivity()).start();
        getActivity().finish();
    }

    @UiThread
    void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void startProgress() {
        if (getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if (getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }
}



