package otgmoblie.com.br.medko.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

/**
 * Created by Thialyson on 06/09/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Place {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty
    private String address;

    @DatabaseField
    @JsonProperty
    private String complement;

    @DatabaseField
    @JsonProperty
    private Float latitude;

    @DatabaseField
    @JsonProperty
    private Float longitude;

    @ForeignCollectionField
    @JsonProperty("attendence")
    private Collection<Attendence> attendences;


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getComplement() { return complement; }

    public void setComplement(String complement) { this.complement = complement; }

    public Float getLatitude() { return latitude; }

    public void setLatitude(Float latitude) { this.latitude = latitude; }

    public Float getLongitude() { return longitude; }

    public void setLongitude(Float longitude) { this.longitude = longitude; }

    public Collection<Attendence> getAttendences() { return attendences; }

    public void setAttendences(Collection<Attendence> attendences) { this.attendences = attendences; }
}
