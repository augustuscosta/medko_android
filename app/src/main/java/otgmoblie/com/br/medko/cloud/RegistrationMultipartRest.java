package otgmoblie.com.br.medko.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.androidannotations.rest.spring.api.RestClientHeaders;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.MultiValueMap;

import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.util.RestUtil;


/**
 * Created by augustuscosta on 31/08/16.
 */
@Rest(rootUrl = RestUtil.ROOT_URL, converters = {ByteArrayHttpMessageConverter.class, FormHttpMessageConverter.class, MappingJackson2HttpMessageConverter.class, StringHttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface RegistrationMultipartRest extends RestClientHeaders {


    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("users/register_mobile_multipart")
    User uploadImage(@Body MultiValueMap<String, Object> data);

    String getCookie(String name);

    void setCookie(String name, String value);
}
