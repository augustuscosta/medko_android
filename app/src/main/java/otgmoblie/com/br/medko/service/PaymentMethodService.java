package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import otgmoblie.com.br.medko.cloud.PaymentMethodRest;
import otgmoblie.com.br.medko.model.PaymentMethod;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 07/10/16.
 */
@EBean
public class PaymentMethodService {
    @RootContext
    Context context;

    @RestService
    PaymentMethodRest paymentMethodRest;

    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<PaymentMethod, Integer> paymentMethodDao;

    public List<PaymentMethod> getAll() {

        try{
            return paymentMethodDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<PaymentMethod> request() throws Exception {

        List<PaymentMethod> toReturn = null;

        try {
            paymentMethodRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = paymentMethodRest.getAll();
            clearTable();
            persist(toReturn);
            cookieService.storedCookie(paymentMethodRest.getCookie(RestUtil.MOBILE_COOKIE));

        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    private void persist(PaymentMethod paymentMethod){
        try{
            paymentMethodDao.createOrUpdate(paymentMethod);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void persist(List<PaymentMethod> paymentMethods){
        if(paymentMethods == null)
            return;

        for(PaymentMethod paymentMethod:paymentMethods){
            persist(paymentMethod);
        }
    }

    public PaymentMethod get(Integer id) {
        try{
            return paymentMethodDao.queryForId(id);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(paymentMethodDao.getConnectionSource(), PaymentMethod.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
