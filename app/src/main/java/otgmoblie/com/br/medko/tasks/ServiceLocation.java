package otgmoblie.com.br.medko.tasks;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import otgmoblie.com.br.medko.model.Point;
import otgmoblie.com.br.medko.service.AvalaibleService;
import otgmoblie.com.br.medko.service.PointService;
import otgmoblie.com.br.medko.service.SessionService;

/**
 * Created by Thialyson on 22/08/16.
 */
@EService
public class ServiceLocation extends Service {

    @Bean
    AvalaibleService avalaibleService;

    @Bean
    PointService pointService;

    @Bean
    SessionService sessionService;

    MyLocation myLocation;

    Point point;

    static Thread threadRunning;

    private int intervalSendPoint = 300000;

    @Override
    public void onCreate() {
        super.onCreate();
        point = new Point();
    }

    public void generateCoordinates() {
        myLocation = new MyLocation(this);
    }


    @Override
    public int onStartCommand(Intent i, int flags, int startId) {

        generateCoordinates();
        if (threadRunning == null) {
            threadRunning = new Thread() {
                @Override
                public void run() {

                    while (avalaibleService.getSharedPreferencesAvailable(ServiceLocation.this) && (sessionService.getCurrentSession() != null))

                        try {

                            if(point.getLatitude() != 0.00){
                                pointService.sendLocation(point);
                            }

                            Thread.sleep(intervalSendPoint);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
            };
            threadRunning.start();

        }
        return super.onStartCommand(i, flags, startId);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {

        try {
            super.onDestroy();
            avalaibleService.updateSharedPreferencesAvailable(false);
            threadRunning = null;
        } catch (Exception ie) {
            ie.printStackTrace();
        }
    }


    public class MyLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

        private GoogleApiClient googleApiClient;

        MyLocation(Context context) {

            if (googleApiClient == null) {
                googleApiClient = new GoogleApiClient.Builder(context)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
            }

            googleApiClient.connect();

        }

        private void getLocation(Location location) {

            if (location == null)
                return;

            point.setLatitude(location.getLatitude());
            point.setLongitude(location.getLongitude());

        }

        @Override
        public void onLocationChanged(Location location) {
            if (getApplicationContext() != null) {

                if (avalaibleService.getSharedPreferencesAvailable(ServiceLocation.this)) {
                    getLocation(location);
                } else {
                    stopGetLocation();
                }
            }
        }

        @Override
        public void onConnected(Bundle bundle) {

            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(intervalSendPoint);
            locationRequest.setFastestInterval(intervalSendPoint);

            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;

            }else {
                getLocation(LocationServices.FusedLocationApi.getLastLocation(googleApiClient));
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            }

        }


        @Override
        public void onConnectionSuspended(int i) { }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) { }

        void stopGetLocation() {

            if (googleApiClient.isConnected() && getApplicationContext() != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
                googleApiClient.disconnect();
            }
        }

    }
}
