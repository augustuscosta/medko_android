package otgmoblie.com.br.medko.views.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mvc.imagepicker.ImagePicker;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.service.DeviceIdService;
import otgmoblie.com.br.medko.service.RegistrationService;
import otgmoblie.com.br.medko.service.SessionService;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.util.ValidateUtil;
import otgmoblie.com.br.medko.views.home.HomeActivity_;


/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_register_patient)
public class RegisterPatientFragment extends Fragment {

    @ViewById(R.id.imageViewRegisterUser)
    de.hdodenhof.circleimageview.CircleImageView avatarImages;

    @ViewById(R.id.editTextName)
    EditText name;

    @ViewById(R.id.editTextEmail)
    EditText email;

    @ViewById(R.id.editTextPhone)
    EditText telephone;

    @ViewById(R.id.editTextPassword)
    EditText password;

    @ViewById(R.id.editTextConfirmationPassword)
    EditText passwordConfirmation;

    @ViewById(R.id.textViewTermsAndServices)
    TextView termsAndConditions;

    @Bean
    SessionService sessionService;

    @Bean
    RegistrationService registrationService;

    @Bean
    DeviceIdService deviceIdService;

    private Bitmap avatarBitmap;

    private ProgressDialog progress;

    private static final int PICK_IMAGE_ID = 234;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 5;


    @Click(R.id.buttonRegisterPatient)
    void registrar() {

        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.checkInterval(name, 1, 50, getString(R.string.invalid_name)));
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        checkedList.add(ValidateUtil.checkInterval(password, 6, 18, getString(R.string.invalid_password)));
        checkedList.add(ValidateUtil.checkConfirmPassword(password, passwordConfirmation, getString(R.string.diferent_passwords)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            startProgressDialog();
            register();
            closeProgressDialog();
        }

    }

    @UiThread
    void startProgressDialog() {
        progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgressDialog() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

    @Background
    void register() {
        getImageAndRegister(avatarBitmap);
    }

    public User returnUser(User user) {
        user.setName(name.getText().toString());
        user.setEmail(email.getText().toString());
        user.setTelephone(telephone.getText().toString());
        user.setPassword(password.getText().toString());
        user.setPasswordConfirmation(passwordConfirmation.getText().toString());
        user.setActive(true);
        user.setAccountTypeId(1);

        return user;
    }

    @Background
    void getImageAndRegister(Bitmap avatarBitmap) {

        User user = returnUser(new User());
        FileSystemResource imageAvatar;

        if (avatarBitmap != null) {
            imageAvatar = new FileSystemResource(persistImage(avatarBitmap, getString(R.string.avatar)));

            startProgress();

            if (registrationService.register(user, imageAvatar, null, null) != null) {
                closeProgress();
                callHomeActivity();
            } else {
                closeProgress();
                toastErrorOnRegister();
            }

        } else {
            if (registrationService.register(user)) {
                startProgress();
                callHomeActivity();
            } else {
                closeProgress();
                toastErrorOnRegister();
            }
        }

    }

    void callHomeActivity() {
        deviceIdService.sendDeviceId();
        HomeActivity_.intent(this).start();
        getActivity().finish();
    }

    @UiThread
    void toastErrorOnRegister() {
        Toast.makeText(getActivity(), R.string.could_not_register, Toast.LENGTH_LONG).show();
    }

    private File persistImage(Bitmap bitmap, String name) {
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            return imageFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Click(R.id.buttonAvatarImagePatient)
    void takeAPickAvatarImagePatient(View view) {
        checkPermission(PICK_IMAGE_ID);
    }


    @Background
    public void checkPermission(Integer integer) {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
            onPickImage(new View(getActivity()), integer);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onPickImage(new View(getActivity()), requestCode);
                }
                break;
        }
    }

    public void onPickImage(View view, Integer integer) {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity(), getString(R.string.choose));
        startActivityForResult(chooseImageIntent, integer);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = ImagePicker.getImageFromResult(getActivity(), requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_ID:

                if (bitmap != null) {
                    avatarImages.setBackground(null);
                    avatarImages.setImageBitmap(bitmap);
                    avatarBitmap = bitmap;
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    @Click(R.id.textViewTermsAndServices)
    void goToTermsAndServices() {
        termsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @UiThread
    void startProgress() {
        if (getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if (getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }

}
