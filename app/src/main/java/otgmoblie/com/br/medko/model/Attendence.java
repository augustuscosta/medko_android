package otgmoblie.com.br.medko.model;



import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.Date;

/**
 * Created by augustuscosta on 22/06/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Attendence{

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField(dataType = DataType.DATE)
    @JsonProperty
    private Date date;

    @DatabaseField
    @JsonProperty
    private Float value;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Place place;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty("search_address")
    private SearchAddress searchAddress;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty("payment_method")
    private PaymentMethod paymentMethod;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty("attendence_type")
    private AttendenceType attendenceType;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty("attendence_status")
    private AttendenceStatus attendenceStatus;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Payment payment;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty("health_plan")
    private HealthPlan healthPlan;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Classification classification;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private User patient;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private User doctor;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Speciality speciality;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty("exam_type")
    private ExamType examType;

    @ForeignCollectionField(eager = true)
    @JsonProperty("take_time")
    private Collection<TakeTime> takeTimes;



    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public Float getValue() { return value; }

    public void setValue(Float value) { this.value = value; }

    public Place getPlace() { return place; }

    public void setPlace(Place place) { this.place = place; }

    public SearchAddress getSearchAddress() { return searchAddress; }

    public void setSearchAddress(SearchAddress searchAddress) { this.searchAddress = searchAddress; }

    public PaymentMethod getPaymentMethod() { return paymentMethod; }

    public void setPaymentMethod(PaymentMethod paymentMethod) { this.paymentMethod = paymentMethod; }

    public AttendenceType getAttendenceType() { return attendenceType; }

    public void setAttendenceType(AttendenceType attendenceType) { this.attendenceType = attendenceType; }

    public AttendenceStatus getAttendenceStatus() { return attendenceStatus; }

    public void setAttendenceStatus(AttendenceStatus attendenceStatus) { this.attendenceStatus = attendenceStatus; }

    public Payment getPayment() { return payment; }

    public void setPayment(Payment payment) { this.payment = payment; }

    public HealthPlan getHealthPlan() { return healthPlan; }

    public void setHealthPlan(HealthPlan healthPlan) { this.healthPlan = healthPlan; }

    public Classification getClassification() { return classification; }

    public void setClassification(Classification classification) { this.classification = classification; }

    public User getPatient() { return patient; }

    public void setPatient(User patient) { this.patient = patient; }

    public User getDoctor() { return doctor; }

    public void setDoctor(User doctor) { this.doctor = doctor; }

    public Speciality getSpeciality() { return speciality; }

    public void setSpeciality(Speciality speciality) { this.speciality = speciality; }

    public ExamType getExamType() { return examType; }

    public void setExamType(ExamType examType) { this.examType = examType; }

    public Collection<TakeTime> getTakeTimes() { return takeTimes; }

    public void setTakeTimes(Collection<TakeTime> takeTimes) { this.takeTimes = takeTimes; }
}
