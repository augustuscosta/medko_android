package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import otgmoblie.com.br.medko.cloud.AttendenceRest;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.AttendenceNewStatus;
import otgmoblie.com.br.medko.model.AttendenceStatus;
import otgmoblie.com.br.medko.model.AttendenceType;
import otgmoblie.com.br.medko.model.TakeTime;
import otgmoblie.com.br.medko.model.Classification;
import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.model.HealthPlan;
import otgmoblie.com.br.medko.model.Payment;
import otgmoblie.com.br.medko.model.PaymentMethod;
import otgmoblie.com.br.medko.model.Place;
import otgmoblie.com.br.medko.model.SearchAddress;
import otgmoblie.com.br.medko.model.Speciality;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class AttendenceService{

    @RootContext
    Context context;

    @RestService
    AttendenceRest attendenceRest;

    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Attendence, Integer> attendenceDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<TakeTime, Integer> attendenceTimeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<User, Integer> userDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Speciality, Integer> specialityDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<ExamType, Integer> examTypeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Classification, Integer> classificationDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<HealthPlan, Integer> healtPlanDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Payment, Integer> paymentDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<AttendenceStatus, Integer> attendenceStatusDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<AttendenceType, Integer> attendenceTypeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<PaymentMethod, Integer> paymentMethodDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<SearchAddress, Integer> searchAddressDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Place, Integer> placeDao;



    public List<Attendence> getAll() {
        try{
            return attendenceDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<Attendence> request() throws Exception {

        List<Attendence> toReturn = null;

        try {
            attendenceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = attendenceRest.getAll();
            persist(toReturn);
            cookieService.storedCookie(attendenceRest.getCookie(RestUtil.MOBILE_COOKIE));
        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    public Attendence request(Integer attendenceId) throws Exception {

        Attendence toReturn = null;

        try {
            attendenceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = attendenceRest.getAttendence(attendenceId);
            persist(toReturn);
            cookieService.storedCookie(attendenceRest.getCookie(RestUtil.MOBILE_COOKIE));
        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    public Attendence searchForId(Integer id) {
        try{
            return attendenceDao.queryForId(id);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public Boolean setAttendenceStatusWithTime(AttendenceNewStatus attendenceNewStatus){

        try{
            attendenceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            attendenceRest.setAttendenceStatusWithTime(attendenceNewStatus);
            cookieService.storedCookie(attendenceRest.getCookie(RestUtil.MOBILE_COOKIE));
            return true;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }
    }

    public Boolean setAttendenceStatusToRealized(AttendenceNewStatus attendenceNewStatus){

        try{
            attendenceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            attendenceRest.setAttendenceStatusToConfirm(attendenceNewStatus);
            cookieService.storedCookie(attendenceRest.getCookie(RestUtil.MOBILE_COOKIE));
            return true;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }
    }

    public Boolean setAttendenceStatusToCanceled(AttendenceNewStatus attendenceNewStatus){

        try{
            attendenceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            attendenceRest.setAttendenceStatusToCancel(attendenceNewStatus);
            cookieService.storedCookie(attendenceRest.getCookie(RestUtil.MOBILE_COOKIE));
            return true;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }
    }

    public Boolean setClassification(Classification classification){

        try{
            attendenceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            attendenceRest.setClassification(classification);
            cookieService.storedCookie(attendenceRest.getCookie(RestUtil.MOBILE_COOKIE));
            return true;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }
    }

    public Boolean setAttendenceStatusToMarked(AttendenceNewStatus AttendenceNewStatus){

        try{
            attendenceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            attendenceRest.setAttendenceStatusToMarked(AttendenceNewStatus);
            cookieService.storedCookie(attendenceRest.getCookie(RestUtil.MOBILE_COOKIE));
            return true;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }
    }

    private void persist(Attendence attendence){
        try{
            persist(attendence.getDoctor());
            persist(attendence.getPatient());
            persist(attendence.getSpeciality());
            persist(attendence.getExamType());
            persist(attendence.getHealthPlan());
            persist(attendence.getClassification());
            persist(attendence.getPayment());
            persist(attendence.getPaymentMethod());
            persist(attendence.getAttendenceType());
            persist(attendence.getAttendenceStatus());
            persist(attendence.getSearchAddress());
            persist(attendence.getPlace());

            attendenceDao.createOrUpdate(attendence);
            persist(attendence, attendence.getTakeTimes());
        }catch (SQLException e){
            e.printStackTrace();
        }
    }


    private void persist(User user) throws SQLException {
        if(user == null)
            return;
        userDao.createOrUpdate(user);
    }

    private void persist(Speciality speciality) throws SQLException {
        if(speciality == null)
            return;
        specialityDao.createOrUpdate(speciality);
    }

    private void persist(ExamType examType) throws SQLException {
        if(examType == null)
            return;
        examTypeDao.createOrUpdate(examType);
    }

    private void persist(Classification classification) throws SQLException {
        if(classification == null)
            return;
        classificationDao.createOrUpdate(classification);
    }

    private void persist(HealthPlan healthPlan) throws SQLException {
        if(healthPlan == null)
            return;
        healtPlanDao.createOrUpdate(healthPlan);
    }

    private void persist(Payment payment) throws SQLException {
        if(payment == null)
            return;
        paymentDao.createOrUpdate(payment);
    }

    private void persist(PaymentMethod paymentMethod) throws SQLException {
        if(paymentMethod == null)
            return;
        paymentMethodDao.createOrUpdate(paymentMethod);
    }

    private void persist(AttendenceType attendenceType) throws SQLException {
        if(attendenceType == null)
            return;
        attendenceTypeDao.createOrUpdate(attendenceType);
    }

    private void persist(AttendenceStatus attendenceStatus) throws SQLException {
        if(attendenceStatus == null)
            return;
        attendenceStatusDao.createOrUpdate(attendenceStatus);
    }

    private void persist(SearchAddress searchAddress) throws SQLException {
        if(searchAddress == null)
            return;
        searchAddressDao.createOrUpdate(searchAddress);
    }

    private void persist(Place place) throws SQLException {
        if(place == null)
            return;
        placeDao.createOrUpdate(place);
    }

    private void persist(TakeTime takeTime) throws SQLException {
        if(takeTime == null)
            return;
        attendenceTimeDao.createOrUpdate(takeTime);
    }


    private void persist(List<Attendence> attendences){
        if(attendences == null)
            return;
        for(Attendence attendence: attendences){
            persist(attendence);
        }
    }

    private void persist(Attendence attendence, Collection<TakeTime> takeTimes) throws SQLException {
        if(takeTimes == null)
            return;
        clearAttendenceTakeTimes(attendence);

        for(TakeTime takeTime : takeTimes){
            takeTime.setAttendence(attendence);
            persist(takeTime);
        }
    }

    private void clearAttendenceTakeTimes(Attendence attendence) throws SQLException {
        DeleteBuilder<TakeTime, Integer> deleteBuilder = attendenceTimeDao.deleteBuilder();
        deleteBuilder.where().eq("attendence_id", attendence.getId());
        deleteBuilder.delete();
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(attendenceDao.getConnectionSource(), Attendence.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
