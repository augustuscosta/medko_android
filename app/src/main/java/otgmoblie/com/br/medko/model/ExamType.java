package otgmoblie.com.br.medko.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

/**
 * Created by Thialyson on 28/07/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class ExamType{

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty("home_care")
    private boolean homeCare;

    @JsonProperty
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private User user;

    @ForeignCollectionField
    @JsonProperty("attendence")
    private Collection<Attendence> attendences;

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String nome) { name = nome; }

    public boolean isHomeCare() { return homeCare; }

    public void setHomeCare(boolean homeCare) { this.homeCare = homeCare; }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collection<Attendence> getAttendences() { return attendences; }

    public void setAttendences(Collection<Attendence> attendences) { this.attendences = attendences; }
}
