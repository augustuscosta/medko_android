package otgmoblie.com.br.medko.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Thialyson on 17/10/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchParameters {

    @JsonProperty("attendence_type")
    private String attendenceType;

    @JsonProperty("health_plan")
    private String healthPlan;

    @JsonProperty("payment")
    private String paymentMethod;

    @JsonProperty("exam")
    private String examType;

    @JsonProperty
    private String speciality;

    @JsonProperty
    private String latitude;

    @JsonProperty
    private String longitude;

    @JsonProperty
    private String address;


    public String getAttendenceType() { return attendenceType; }

    public void setAttendenceType(String attendenceType) { this.attendenceType = attendenceType; }

    public String getHealthPlan() { return healthPlan; }

    public void setHealthPlan(String healthPlan) { this.healthPlan = healthPlan; }

    public String getPaymentMethod() { return paymentMethod; }

    public void setPaymentMethod(String paymentMethod) { this.paymentMethod = paymentMethod; }

    public String getExamType() { return examType; }

    public void setExamType(String examType) { this.examType = examType; }

    public String getSpeciality() { return speciality; }

    public void setSpeciality(String speciality) { this.speciality = speciality; }

    public String getLatitude() { return latitude; }

    public void setLatitude(String latitude) { this.latitude = latitude; }

    public String getLongitude() { return longitude; }

    public void setLongitude(String longitude) { this.longitude = longitude; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }
}
