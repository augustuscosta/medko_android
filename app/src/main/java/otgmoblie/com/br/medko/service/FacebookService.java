package otgmoblie.com.br.medko.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Collections;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.util.DialogUtil;

/**
 * Created by augustuscosta on 11/07/16.
 */
public class FacebookService implements FacebookCallback<LoginResult> {

    Context context;

    private FacebookServiceCallback serviceCallback;

    public CallbackManager callbackManager;
    private ProfileTracker mProfileTracker;

    public void init(Context context, FacebookServiceCallback serviceCallback){
        this.serviceCallback = serviceCallback;
        this.context = context;
        FacebookSdk.sdkInitialize(context);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    public void loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions((Activity) context, Collections.singletonList("public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        final String token = loginResult.getAccessToken().getToken();
        Profile.fetchProfileForCurrentAccessToken();

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {

                    final String emailFacebook = object.getString("email");

                    if (emailFacebook != null && !emailFacebook.isEmpty()) {

                        Profile usuarioFacebook = Profile.getCurrentProfile();

                        if (usuarioFacebook != null) {
                            setDadosFacebookProfile(token, usuarioFacebook, emailFacebook);
                        } else {

                            mProfileTracker = new ProfileTracker() {
                                @Override
                                protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                                    if (profile2 != null) {
                                        if (!isLoggedFacebook()) {
                                            setDadosFacebookProfile(token, profile2, emailFacebook);
                                            mProfileTracker.stopTracking();
                                        }
                                    }
                                }
                            };

                            mProfileTracker.startTracking();
                        }
                    }

                } catch (Exception e) {
                    DialogUtil.createDialogFacebookSemEmail(context);
                    e.printStackTrace();
                }
            }
        });
        setFacebookPermissionsAndStartRequest(request);
    }

    @Override
    public void onCancel() {
        Log.i("Erro:", "Facebook cancelado");
        saveSharedPreferencesTokenFacebook("", context);
        serviceCallback.onFacebookLoginError("Facebook cancelado");
    }

    @Override
    public void onError(FacebookException error) {
        Log.i("Erro:", error.getMessage());
        saveSharedPreferencesTokenFacebook("", context);
        serviceCallback.onFacebookLoginError(error.getMessage());
    }

    private void setDadosFacebookProfile(String token, Profile usuarioFacebook, String email) {

        saveSharedPreferencesTokenFacebook(token, context);
        User user = new User();
        user.setFacebookAccount(true);
        user.setName(usuarioFacebook.getFirstName() + " " + usuarioFacebook.getLastName());
        Uri urlImage = usuarioFacebook.getProfilePictureUri(200, 200);
        user.setAvatar(urlImage.toString());
        user.setEmail(email);
        user.setFacebookToken(token);

        serviceCallback.onFacebookLoginSuccess(user);
    }

    private void setFacebookPermissionsAndStartRequest(GraphRequest request) {
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public static void saveSharedPreferencesTokenFacebook(String tokenFacebook, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("FACEBOOK_TOKEN", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("FACEBOOK_TOKEN", tokenFacebook);
        editor.apply();
    }

    public static String getSharedPreferencesTokenFacebook(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("FACEBOOK_TOKEN", Context.MODE_PRIVATE);
        String token = sharedPref.getString("FACEBOOK_TOKEN", "");
        return token;
    }

    public boolean isLoggedFacebook() {
        AccessToken token = AccessToken.getCurrentAccessToken();
        return (token != null || !getSharedPreferencesTokenFacebook(context).isEmpty());
    }
}
