package otgmoblie.com.br.medko.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import otgmoblie.com.br.medko.R;

/**
 * Created by augustuscosta on 12/07/16.
 */
public class DialogUtil {

    public static void createDialogFacebookSemEmail(Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String texto = context.getString(R.string.error_facebook_sem_email);
        builder.setMessage(texto);
        builder.setNegativeButton(R.string.ok, getNegativeButton());
        builder.create().show();
    }

    private static DialogInterface.OnClickListener getNegativeButton() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
    }

    public static AlertDialog.Builder createDialog(Context context,String message) {
        return new AlertDialog.Builder(context, R.style.MyDialogTheme)
                .setCancelable(false)
                .setMessage(message);
    }

    public static AlertDialog.Builder getNegativeButton(AlertDialog.Builder builder){
        builder.setNegativeButton(R.string.cancel,getNegativeButton());
        return builder;
    }

}
