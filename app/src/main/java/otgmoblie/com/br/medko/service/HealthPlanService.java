package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import otgmoblie.com.br.medko.cloud.HealthPlanRest;
import otgmoblie.com.br.medko.model.HealthPlan;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 07/10/16.
 */
@EBean
public class HealthPlanService {
    @RootContext
    Context context;

    @RestService
    HealthPlanRest healthPlanRest;

    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<HealthPlan, Integer> healthPlanDao;

    public List<HealthPlan> getAll() {

        try{
            return healthPlanDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<HealthPlan> request() throws Exception {

        List<HealthPlan> toReturn = null;

        try {
            healthPlanRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = healthPlanRest.getAll();
            clearTable();
            persist(toReturn);
            cookieService.storedCookie(healthPlanRest.getCookie(RestUtil.MOBILE_COOKIE));

        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    private void persist(HealthPlan healthPlan){
        try{
            healthPlanDao.createOrUpdate(healthPlan);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void persist(List<HealthPlan> healthPlans){
        if(healthPlans == null)
            return;

        for(HealthPlan healthPlan:healthPlans){
            persist(healthPlan);
        }
    }

    public HealthPlan get(Integer id) {
        try{
            return healthPlanDao.queryForId(id);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(healthPlanDao.getConnectionSource(), HealthPlan.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
