package otgmoblie.com.br.medko.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by augustuscosta on 06/07/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWrapper {

    @JsonProperty
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
