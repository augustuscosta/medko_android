package otgmoblie.com.br.medko.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.List;

/**
 * Created by Thialyson on 10/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceWindows {


    @JsonProperty
    private Integer id;

    @JsonProperty("user")
    private User doctor;

    @JsonProperty
    private Float value;

    @JsonProperty
    private Integer media;

    @JsonProperty
    private Boolean rating;

    @JsonProperty
    private Place place;

    @JsonProperty
    private Speciality speciality;

    @JsonProperty("exam_type")
    private ExamType examType;

    @JsonProperty("take_time")
    private String takeTime;

    @JsonProperty("health_plan")
    private List<HealthPlan> healthPlan;


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public User getDoctor() { return doctor; }

    public void setDoctor(User doctor) { this.doctor = doctor; }

    public Float getValue() { return value; }

    public Integer getMedia() {
        return media;
    }

    public void setMedia(Integer media) {
        this.media = media;
    }

    public Boolean getRating() {
        return rating;
    }

    public void setRating(Boolean rating) {
        this.rating = rating;
    }

    public void setValue(Float value) { this.value = value; }

    public Place getPlace() { return place; }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public ExamType getExamType() {
        return examType;
    }

    public void setExamType(ExamType examType) {
        this.examType = examType;
    }

    public String getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(String takeTime) {
        this.takeTime = takeTime;
    }

    public List<HealthPlan> getHealthPlan() {
        return healthPlan;
    }

    public void setHealthPlan(List<HealthPlan> healthPlan) {
        this.healthPlan = healthPlan;
    }
}
