package otgmoblie.com.br.medko.views.servicewindows;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.Objects;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.SearchParameters;
import otgmoblie.com.br.medko.model.SelectedIds;
import otgmoblie.com.br.medko.model.ServiceWindows;
import otgmoblie.com.br.medko.service.CurrentUserAccountTypeIdService;
import otgmoblie.com.br.medko.service.ServiceWindowsService;
import otgmoblie.com.br.medko.util.DialogUtil;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.views.attendence.AttendenceActivity_;
import otgmoblie.com.br.medko.views.attendence.NewAttendenceFragment_;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_service_windows_list)
public class ServiceWindowsListFragment extends Fragment implements AdapterView.OnItemClickListener {

    @ViewById(R.id.recyclerViewServiceWindows)
    RecyclerView recyclerView;

    @ViewById(R.id.noServiceWindowsMarked)
    TextView textNoServiceWindowsMarked;

    @Bean
    ServiceWindowsService serviceWindowsService;

    @Bean
    CurrentUserAccountTypeIdService currentUserAccountTypeIdService;

    ServiceWindowsListAdapter serviceWindowsListAdapter;

    String attendenceTypeId, healthPlanId, healthPlanName, paymentMethodId, specialityId, examTypeId, serviceTypeId, latitude, longitude;

    ProgressDialog progress;

    Bundle bundle;

    List<ServiceWindows> serviceWindowsList;
    private SelectedIds selectedIds;


    @AfterViews
    void afterViews() {
        bundle = getArguments();
        getIdsFromArguments();
    }

    @UiThread
    void getIdsFromArguments() {
        if (bundle != null) {
            attendenceTypeId = bundle.getString("attendenceTypeId");
            healthPlanId = bundle.getString("healthPlanId");
            healthPlanName = bundle.getString("healthPlanName");
            paymentMethodId = bundle.getString("paymentMethodId");
            specialityId = bundle.getString("specialityId");
            examTypeId = bundle.getString("examTypeId");
            serviceTypeId = bundle.getString("serviceTypeId");
            latitude = bundle.getString("latitude");
            longitude = bundle.getString("longitude");

            setSearchParametersAttributes();
        }
    }

    @UiThread
    void setSearchParametersAttributes() {
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setAttendenceType(getParam(attendenceTypeId));
        searchParameters.setHealthPlan(getParam(healthPlanId));
        searchParameters.setPaymentMethod(getParam(paymentMethodId));
        searchParameters.setSpeciality(getParam(specialityId));
        searchParameters.setExamType(getParam(examTypeId));
        searchParameters.setLatitude(getParam(latitude));
        searchParameters.setLongitude(getParam(longitude));

        requestListServiceWindows(searchParameters);
    }

    @SuppressLint("NewApi")
    private String getParam(String id) {
        if (id != null && !Objects.equals(id, "")) {
            return id;
        } else {
            return "";
        }
    }

    @Background
    void requestListServiceWindows(SearchParameters searchParameters) {
        startProgress();

        try {
            serviceWindowsList = serviceWindowsService.getServiceWindowsList(searchParameters);
            closeProgress();
            setAdapterRecyclerView();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }

        checkIfListAttendenceListIsNull(serviceWindowsList);

    }

    @UiThread
    void checkIfListAttendenceListIsNull(List<ServiceWindows> serviceWindowsList) {
        if (serviceWindowsList.size() == 0) {
            textNoServiceWindowsMarked.setVisibility(View.VISIBLE);
            callDialogAttendenceNotFound();
        } else {
            textNoServiceWindowsMarked.setVisibility(View.GONE);
        }
    }

    @UiThread
    void callDialogAttendenceNotFound() {
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.attendece_not_found));
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        switchFragment(new NewAttendenceFragment_());
                    }
                });
        builder.show();
    }

    @UiThread
    void setAdapterRecyclerView() {
        if (serviceWindowsList.size() > 0) {

            if (serviceWindowsListAdapter == null) {
                serviceWindowsListAdapter = new ServiceWindowsListAdapter(serviceWindowsList, getActivity(), serviceTypeId, healthPlanId, healthPlanName, this);
            }
            recyclerView.setAdapter(serviceWindowsListAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            serviceWindowsListAdapter.notifyDataSetChanged();
        }
    }


    @UiThread
    void startProgress() {
        if(getActivity() != null)
        progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if(getActivity() != null)
        ProgressDialogUtil.closeProgressDialog(progress);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SelectedIds selectedIds = new SelectedIds();
        selectedIds.setServiceWindowsId(serviceWindowsList.get(position).getId());
        choosePaymentMethod(selectedIds);
    }

    @SuppressLint("NewApi")
    public void choosePaymentMethod(SelectedIds selectedIds) {

        if (healthPlanId != null && !Objects.equals(healthPlanId, "")) {
            selectedIds.setHealthPlanId(Integer.valueOf(healthPlanId));

        }else if (paymentMethodId != null && !Objects.equals(paymentMethodId, "")){
            selectedIds.setPaymentMethodId(Integer.valueOf(paymentMethodId));
        }
        sendIdServiceWindows(selectedIds);
    }


    @Background
    void sendIdServiceWindows(SelectedIds selectedIds) {
        if (selectedIds != null) {
            Attendence newAttendence = null;
            startProgress();
            try {
                newAttendence = serviceWindowsService.getServiceWindowsClinic(selectedIds);
                closeProgress();
            } catch (Exception e) {
                closeProgress();
                e.printStackTrace();
            }

            callAttendenceActivity(newAttendence);
        }
    }

    @UiThread
    void callAttendenceActivity(Attendence newAttendence) {
        if (newAttendence != null) {
            AttendenceActivity_.intent(getActivity()).attendenceId(newAttendence.getId()).start();
            getActivity().finish();
        }
    }

    @UiThread
    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment);
        transaction.commit();
    }

}
