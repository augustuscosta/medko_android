package otgmoblie.com.br.medko;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;

/**
 * Created by augustuscosta on 14/07/16.
 */
public class AppController extends Application {

    private static AppController mInstance;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        printKeyHash();
    }

    public void printKeyHash() {
        try {

            PackageInfo info = getPackageManager().getPackageInfo("otgmoblie.com.br.medko", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("keyHash->>>", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
