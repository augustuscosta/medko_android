package otgmoblie.com.br.medko.views.attendence;


import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Collections;
import java.util.List;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.service.AttendenceService;
import otgmoblie.com.br.medko.service.CurrentUserAccountTypeIdService;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;

/**
 * A simple {@link Fragment} subclass.
 */

@EFragment(R.layout.fragment_attendence_list)
public class AttendenceListFragment extends Fragment {

    @ViewById(R.id.recyclerViewListFragment)
    RecyclerView recyclerViewListFragment;

    @ViewById(R.id.noAttendencesMarked)
    TextView textNoAttendenceMarked;

    @ViewById(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @Bean
    AttendenceService attendenceService;

    @Bean
    CurrentUserAccountTypeIdService currentUserAccountTypeIdService;

    private ProgressDialog progress;

    private Integer accountTypeCurrentUser;


    @AfterViews
    void afterViews() {

        if (attendenceService.getAll() != null && attendenceService.getAll().size() > 0) {
            getValuesFromDatabase();
        } else {
            loadComponents();
        }

        listenRefresh();
    }

    void listenRefresh() {
        swipeRefresh.setColorSchemeResources(R.color.tab_background_green);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadComponents();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Background()
    void loadComponents() {
        startProgress();

        try {
            attendenceService.request();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        closeProgress();

        getValuesFromDatabase();
    }

    void getValuesFromDatabase() {
        if (attendenceService.getAll() != null) {
            getAccountTypeCurrentUser();
            setAdapterRecyclerView();
            checkIfListAttendenceListIsNull();
        }
    }

    @UiThread
    void getAccountTypeCurrentUser() {
        if (currentUserAccountTypeIdService.getSharedPreferencesCurrentUserId(getActivity()) != 0) {
            accountTypeCurrentUser = currentUserAccountTypeIdService.getSharedPreferencesCurrentUserId(getActivity());
        }
    }

    @UiThread
    void checkIfListAttendenceListIsNull() {
        if (attendenceService.getAll().size() == 0) {
            textNoAttendenceMarked.setVisibility(View.VISIBLE);
        } else {
            textNoAttendenceMarked.setVisibility(View.GONE);
        }
    }


    @UiThread
    void setAdapterRecyclerView() {
        List<Attendence> listReversed = attendenceService.getAll();
        Collections.reverse(listReversed);
        AttendenceListAdapter attendenceListAdapter = new AttendenceListAdapter(listReversed, getActivity(), accountTypeCurrentUser);
        recyclerViewListFragment.setAdapter(attendenceListAdapter);
        recyclerViewListFragment.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        attendenceListAdapter.notifyDataSetChanged();
    }

    @UiThread
    void startProgress() {
        if(getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if(getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }

}
