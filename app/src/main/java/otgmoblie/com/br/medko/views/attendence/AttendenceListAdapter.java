package otgmoblie.com.br.medko.views.attendence;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Attendence;

/**
 * Created by Thialyson on 05/09/16.
 */


class AttendenceListAdapter extends RecyclerView.Adapter<AttendenceListAdapter.ViewHolder> {

    private List<Attendence> attendenceList;
    private LayoutInflater layoutInflater;
    private Context context;
    private Integer accountTypeCurrentUser;

    AttendenceListAdapter(List<Attendence> attendenceList, Context context, Integer accountTypeCurrentUser) {
        if(context != null) {
            this.attendenceList = attendenceList;
            this.context = context;
            this.accountTypeCurrentUser = accountTypeCurrentUser;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_attendence, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        setSpeciality(holder, position);
        setExamType(holder, position);
        setRatingBar(holder, position);
        setDate(holder, position);
        setPersonalAttributes(holder, position);
        setProprietyStatusAttendence(holder, position);
    }

    private void setSpeciality(ViewHolder holder, int position) {
        if (attendenceList != null) {
            if (attendenceList.get(position).getSpeciality() != null) {
                holder.textViewSpeciality.setVisibility(View.VISIBLE);
                holder.textViewSpeciality.setText(attendenceList.get(position).getSpeciality().getName());
            } else {
                holder.textViewSpeciality.setVisibility(View.GONE);
            }
        }
    }

    private void setExamType(ViewHolder holder, int position) {
        if (attendenceList.get(position).getExamType() != null) {
            holder.textViewExamType.setVisibility(View.VISIBLE);
            holder.textViewExamType.setText(attendenceList.get(position).getExamType().getName());
        } else {
            holder.textViewExamType.setVisibility(View.GONE);
        }
    }

    private void setRatingBar(ViewHolder holder, int position) {
        if (accountTypeCurrentUser == 1 && attendenceList.get(position).getDoctor().getRating() != null && attendenceList.get(position).getDoctor().getRating() && attendenceList.get(position).getDoctor().getMedia() > 0) {
            holder.ratingBar.setVisibility(View.VISIBLE);
            holder.ratingBar.setRating(attendenceList.get(position).getDoctor().getMedia());
        } else {
            holder.textViewSpeciality.setVisibility(View.GONE);
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void setDate(ViewHolder holder, int position) {
        if (attendenceList.get(position).getDate() != null) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            holder.textViewDateTimeAttendence.setText(dateFormat.format(attendenceList.get(position).getDate()));
        } else {
            holder.textViewDateTimeAttendence.setText(R.string.not_confirmed);
        }
    }

    private void setPersonalAttributes(ViewHolder holder, int position) {
        switch (accountTypeCurrentUser) {
            case 1:
                holder.imageUser.setImageResource(R.drawable.ic_doctor);
                holder.textViewNameUser.setText(attendenceList.get(position).getDoctor().getName());
                break;
            case 3:
                holder.imageUser.setImageResource(R.drawable.ic_user);
                holder.textViewNameUser.setText(attendenceList.get(position).getPatient().getName());
                break;
        }
        holder.textViewNameUser.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        setLocalAttendence(holder, position);
    }


    private void setLocalAttendence(ViewHolder holder, int position) {
        if (attendenceList.get(position).getPlace() != null) {
            holder.textViewPlaceAttendence.setText(attendenceList.get(position).getPlace().getName());
        } else {
            holder.textViewPlaceAttendence.setText(R.string.residence);
        }
    }

    private void setProprietyStatusAttendence(ViewHolder holder, int position) {
        holder.textViewStatusAttendence.setText(attendenceList.get(position).getAttendenceStatus().getName());
        switch (attendenceList.get(position).getAttendenceStatus().getId()) {
            case 1:
                holder.textViewStatusAttendence.setTextColor(ContextCompat.getColor(context, R.color.tab_background_green));
                break;
            case 2:
                holder.textViewStatusAttendence.setTextColor(ContextCompat.getColor(context, R.color.tab_background_selected));
                break;
            case 3:
                holder.textViewStatusAttendence.setTextColor(ContextCompat.getColor(context, R.color.tab_background_selected));
                break;
            case 4:
                holder.textViewStatusAttendence.setTextColor(ContextCompat.getColor(context, R.color.background_hint));
                break;
            case 5:
                holder.textViewStatusAttendence.setTextColor(ContextCompat.getColor(context, R.color.tab_background_green));
                break;
            case 6:
                holder.textViewStatusAttendence.setTextColor(ContextCompat.getColor(context, R.color.background_hint));
                break;
            case 7:
                holder.textViewStatusAttendence.setTextColor(ContextCompat.getColor(context, R.color.background_hint));
                break;

        }
    }


    @Override
    public int getItemCount() {
        return attendenceList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        RatingBar ratingBar;
        ImageView imageUser;
        TextView textViewNameUser, textViewExamType, textViewSpeciality, textViewStatusAttendence,
                textViewPlaceAttendence, textViewDateTimeAttendence;

        ViewHolder(final View itemView) {
            super(itemView);

            imageUser = (ImageView) itemView.findViewById(R.id.imageUserItem);
            textViewNameUser = (TextView) itemView.findViewById(R.id.textViewNameUserItem);
            textViewNameUser.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            textViewExamType = (TextView) itemView.findViewById(R.id.textViewExamItemAttencdence);
            textViewSpeciality = (TextView) itemView.findViewById(R.id.textViewSpecialityItemAttendence);
            textViewStatusAttendence = (TextView) itemView.findViewById(R.id.textViewStatusItemAttendence);
            textViewPlaceAttendence = (TextView) itemView.findViewById(R.id.textViewPlaceItemAttendence);
            textViewDateTimeAttendence = (TextView) itemView.findViewById(R.id.textViewDateTimeItemAttendence);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingItemAttendence);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Integer attendenceId = attendenceList.get(getLayoutPosition()).getId();
                    AttendenceActivity_.intent(context).attendenceId(attendenceId).start();
                }
            });
        }
    }

}
