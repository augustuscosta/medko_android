package otgmoblie.com.br.medko.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import otgmoblie.com.br.medko.model.Session;
import otgmoblie.com.br.medko.model.UserWrapper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by augustuscosta on 23/06/16.
 */
@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface RegistrationRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("users/register_mobile")
    Session register(@Body UserWrapper user);

    String getCookie(String name);

    void setCookie(String name, String value);

}