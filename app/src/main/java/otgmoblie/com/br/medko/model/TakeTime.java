package otgmoblie.com.br.medko.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by Thialyson on 27/09/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class TakeTime {

    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField
    @JsonProperty
    private Date date;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Attendence attendence;



    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public Attendence getAttendence() { return attendence; }

    public void setAttendence(Attendence attendence) { this.attendence = attendence; }

}
