package otgmoblie.com.br.medko.views.recoverpass;


import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Session;
import otgmoblie.com.br.medko.service.SessionService;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_recover_passowrd)
public class RecoverPasswordFragment extends Fragment {

    @ViewById(R.id.editTextSendEmail)
    EditText email;

    @Bean
    SessionService sessionService;

    Session session;

    @Click(R.id.buttonSendEmail)
    void resetPasswordButton() {
        resetPassword();
    }

    @Background
    void resetPassword() {
        session = new Session();
        session.setEmail(email.getText().toString());
        if (sessionService.resetPassword(session))
            showResetPasswordToast();
        else
            emailNotFound();
    }

    @UiThread
    void showResetPasswordToast() {
        Toast.makeText(getActivity(), R.string.msg_to_recover_password, Toast.LENGTH_LONG).show();
    }

    @UiThread
    void emailNotFound() {
        Toast.makeText(getActivity(), R.string.email_not_found, Toast.LENGTH_LONG).show();
    }

}
