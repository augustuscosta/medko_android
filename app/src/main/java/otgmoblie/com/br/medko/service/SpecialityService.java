package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import otgmoblie.com.br.medko.cloud.SpecialityRest;
import otgmoblie.com.br.medko.model.Speciality;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 28/07/16.
 */
@EBean
public class SpecialityService{

    @RootContext
    Context context;

    @RestService
    SpecialityRest specialityRest;

    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Speciality, Integer> specialityDao;

    public List<Speciality> getAll() {

        try{
            return specialityDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }


    public List<Speciality> getAllSpecialitiesByName() {

        try{
            return specialityDao.queryBuilder().orderBy("name", true).query();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<Speciality> request() throws Exception {

        List<Speciality> toReturn = null;

        try {
            specialityRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = specialityRest.getAll();
            clearTable();
            persist(toReturn);
            cookieService.storedCookie(specialityRest.getCookie(RestUtil.MOBILE_COOKIE));

        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    public Speciality getSpecialityWhereName(String nome) {

        Speciality specialityTemp = new Speciality();
        try{
            QueryBuilder<Speciality, Integer> queryBuilder = specialityDao.queryBuilder();
            specialityTemp = queryBuilder.where().eq("name", nome).queryForFirst();
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return specialityTemp;
    }

    private void persist(Speciality speciality){
        try{
            specialityDao.createOrUpdate(speciality);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void persist(List<Speciality> specialities){
        if(specialities == null)
            return;

        for(Speciality speciality:specialities){
            persist(speciality);
        }
    }

    public Speciality get(Integer id) {
        try{
            return specialityDao.queryForId(id);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(specialityDao.getConnectionSource(), Speciality.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
