package otgmoblie.com.br.medko.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import otgmoblie.com.br.medko.R;

/**
 * Created by Thialyson on 03/08/16.
 */


public class ProgressDialogUtil extends Activity{

    @SuppressLint("NewApi")
    public static ProgressDialog callProgressDialog(Context context) {

        ProgressDialog progressDialog = new ProgressDialog(context, R.style.AppCompatProgressAlertDialogStyle);
        progressDialog.setCancelable(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.create();
        progressDialog.show();
        return  progressDialog;
    }

    public static void closeProgressDialog(ProgressDialog progress){
        progress.dismiss();
    }

}
