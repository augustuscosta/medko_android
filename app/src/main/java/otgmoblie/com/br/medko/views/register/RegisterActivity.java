package otgmoblie.com.br.medko.views.register;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.views.login.LoginActivity_;
import otgmoblie.com.br.medko.views.recoverpass.RecoverPasswordFragment_;

@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity {

    @ViewById(R.id.toolBar)
    Toolbar toolbar;


    @AfterViews
    void afterviews() {
        setSupportActionBar(toolbar);
        getSupportFragmentManager();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.title_action_bar_register));

        Bundle bundle = getIntent().getExtras();
        switch (bundle.getInt(getString(R.string.key))) {
            case 0:
                switchFragment(new RegisterPatientFragment_());
                break;
            case 1:
                switchFragment(new RegisterDoctorFragment_());
                break;
            case 2:
                switchFragment(new RecoverPasswordFragment_());
                getSupportActionBar().setTitle("Redefinir Senha");
                break;
        }
    }

    @UiThread
    public void switchFragment(android.support.v4.app.Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callLoginActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        callLoginActivity();
    }

    @UiThread
    void callLoginActivity() {
        LoginActivity_.intent(this).start();
        finish();
    }
}
