package otgmoblie.com.br.medko.views.attendence;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.AttendenceType;
import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.model.HealthPlan;
import otgmoblie.com.br.medko.model.PaymentMethod;
import otgmoblie.com.br.medko.model.Speciality;
import otgmoblie.com.br.medko.service.AttendenceTypeService;
import otgmoblie.com.br.medko.service.ExamTypeService;
import otgmoblie.com.br.medko.service.HealthPlanService;
import otgmoblie.com.br.medko.service.PaymentMethodService;
import otgmoblie.com.br.medko.service.SpecialityService;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.views.maps.MapsFragmentPatient_;


@EFragment(R.layout.fragment_new_attendence)
public class NewAttendenceFragment extends Fragment implements AdapterView.OnItemSelectedListener {


    @ViewById(R.id.spinnerPlace)
    Spinner spinnerPlace;

    @ViewById(R.id.spinnerPayment)
    Spinner spinnerPayment;

    @ViewById(R.id.spinnerHealthPlan)
    Spinner spinnerHealthPlan;

    @ViewById(R.id.spinnerPaymentMethod)
    Spinner spinnerPaymentMethod;

    @ViewById(R.id.spinnerServiceType)
    Spinner spinnerServiceType;

    @ViewById(R.id.spinnerSpeciality)
    Spinner spinnerSpeciality;

    @ViewById(R.id.spinnerExamType)
    Spinner spinnerExamType;

    @ViewById(R.id.callEmergency)
    TextView callEmergency;

    @ViewById(R.id.layoutPlace)
    LinearLayout layoutPlace;

    @ViewById(R.id.layoutPayment)
    LinearLayout layoutPayment;

    @ViewById(R.id.layoutHealthPlan)
    LinearLayout layoutHealthPlan;

    @ViewById(R.id.layoutPaymentMethodNew)
    LinearLayout layoutPaymentMethod;

    @ViewById(R.id.layoutServiceType)
    LinearLayout layoutServiceType;

    @ViewById(R.id.layoutSpeciality)
    LinearLayout layoutSpeciality;

    @ViewById(R.id.layoutExame)
    LinearLayout layoutExam;

    @Bean
    AttendenceTypeService attendenceTypeService;

    @Bean
    HealthPlanService healthPlanService;

    @Bean
    PaymentMethodService paymentMethodService;

    @Bean
    SpecialityService specialityService;

    @Bean
    ExamTypeService examTypeService;

    private ProgressDialog progress;

    private String attendenceTypeId, healthPlanId, healthPlanName, paymentMethodId, specialityId, examTypeId, serviceTypeId;

    private List<String> specialityNameList = new ArrayList<>(), paymentNameList = new ArrayList<>(),
            examTypeNameList = new ArrayList<>(), healthPlanNameList = new ArrayList<>(),
            serviceTypeNameList = new ArrayList<>(), attendenceTypeNameList = new ArrayList<>(),
            paymentMethodNameList = new ArrayList<>();


    @AfterViews
    void afterViews() {
        loadComponents();
        listenItemSelected();
    }

    @UiThread
    void listenItemSelected() {
        spinnerPlace.setOnItemSelectedListener(NewAttendenceFragment.this);
        spinnerPayment.setOnItemSelectedListener(NewAttendenceFragment.this);
        spinnerHealthPlan.setOnItemSelectedListener(NewAttendenceFragment.this);
        spinnerPaymentMethod.setOnItemSelectedListener(NewAttendenceFragment.this);
        spinnerServiceType.setOnItemSelectedListener(NewAttendenceFragment.this);
        spinnerSpeciality.setOnItemSelectedListener(NewAttendenceFragment.this);
        spinnerExamType.setOnItemSelectedListener(NewAttendenceFragment.this);
    }


    @Background
    void loadComponents() {
        startProgress();
        try {
            attendenceTypeService.request();
            paymentMethodService.request();
            healthPlanService.request();
            specialityService.request();
            examTypeService.request();
            closeProgress();
            populateLists();
        } catch (Exception e1) {
            closeProgress();
            e1.printStackTrace();
        }
    }


    @UiThread
    void populateLists() {
        populateAttendenceType(attendenceTypeService.getAll());
        populatePayment();
        populateHealthPlan(healthPlanService.getAll());
        populatePaymentMethod(paymentMethodService.getAll());
        populateServiceType();
    }

    @UiThread
    void populateAttendenceType(List<AttendenceType> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                attendenceTypeNameList.add(list.get(i).getName());
            }
            createAdapter(attendenceTypeNameList, spinnerPlace);
        }
        spinnerPlace.setSelection(1, true);
    }

    @UiThread
    void populatePayment() {
        paymentNameList.add(getString(R.string.individual));
        paymentNameList.add(getString(R.string.health_plan));
        createAdapter(paymentNameList, spinnerPayment);
    }

    @UiThread
    void populateHealthPlan(List<HealthPlan> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                healthPlanNameList.add(list.get(i).getName());
            }
            createAdapter(healthPlanNameList, spinnerHealthPlan);
        }
    }

    @UiThread
    void populatePaymentMethod(List<PaymentMethod> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                paymentMethodNameList.add(list.get(i).getName());
            }
            createAdapter(paymentMethodNameList, spinnerPaymentMethod);
        }
    }

    @UiThread
    void populateServiceType() {
        serviceTypeNameList.add(getString(R.string.medical_appointment));
        serviceTypeNameList.add(getString(R.string.exam));
        createAdapter(serviceTypeNameList, spinnerServiceType);
    }

    @UiThread
    void populateSpecialities(List<Speciality> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                specialityNameList.add(list.get(i).getName());
            }
            createAdapter(specialityNameList, spinnerSpeciality);
        }
    }

    @UiThread
    void populateHomeCareSpecialities(List<Speciality> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isHomeCare()) {
                    specialityNameList.add(list.get(i).getName());
                }
            }
            createAdapter(specialityNameList, spinnerSpeciality);
        }
    }

    @UiThread
    void populateExamTypes(List<ExamType> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                examTypeNameList.add(list.get(i).getName());
            }
            createAdapter(examTypeNameList, spinnerExamType);
        }
    }

    @UiThread
    void populateHomeCareExamTypes(List<ExamType> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isHomeCare()) {
                    examTypeNameList.add(list.get(i).getName());
                }
            }
            createAdapter(examTypeNameList, spinnerExamType);
        }
    }

    void clearList(List<String> stringList) {
        stringList.clear();
    }

    @UiThread
    void populateHomeCareSpecialityAndExamTypes() {
        clearList(specialityNameList);
        clearList(examTypeNameList);
        populateHomeCareSpecialities(specialityService.getAllSpecialitiesByName());
        populateHomeCareExamTypes(examTypeService.getAllExamTypesByName());
        createAdapter(specialityNameList, spinnerSpeciality);
        createAdapter(examTypeNameList, spinnerExamType);
    }

    @UiThread
    void populateSpecialityAndExamTypes() {
        clearList(specialityNameList);
        clearList(examTypeNameList);
        populateSpecialities(specialityService.getAllSpecialitiesByName());
        populateExamTypes(examTypeService.getAllExamTypesByName());
        createAdapter(specialityNameList, spinnerSpeciality);
        createAdapter(examTypeNameList, spinnerExamType);
    }

    @UiThread
    void createAdapter(List<String> nameList, Spinner spinner) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, nameList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spinnerPlace:
                showSpinnersByPlace(adapterView);
                break;
            case R.id.spinnerPayment:
                showSpinnersByPayment(adapterView);
                break;
            case R.id.spinnerServiceType:
                showSpinnersByServiceType(adapterView);
                break;
        }
    }


    @UiThread
    void showSpinnersByPlace(AdapterView<?> adapterView) {

        if (adapterView.getSelectedItemPosition() == 0) {
            hideSpinners();
            showOnlySpeciality();
            showCallEmergency();
            populateHomeCareSpecialityAndExamTypes();

        } else if (adapterView.getSelectedItemPosition() == 1) {
            hideSpinners();
            showPayment();
            showOnlyPaymentMethod();
            showOnlySpeciality();
            populateSpecialityAndExamTypes();
        }
    }

    @UiThread
    void showPayment(){
        layoutPayment.setVisibility(View.VISIBLE);
    }

    @UiThread
    void hideSpinners() {
        layoutPaymentMethod.setVisibility(View.GONE);
        layoutHealthPlan.setVisibility(View.GONE);
        layoutPayment.setVisibility(View.GONE);
        callEmergency.setVisibility(View.GONE);
    }

    @UiThread
    void showCallEmergency() {
        callEmergency.setVisibility(View.VISIBLE);
    }


    @UiThread
    void showSpinnersByPayment(AdapterView<?> adapterView) {

        if (adapterView.getSelectedItemPosition() == 0 || adapterView.getSelectedItem().equals(getString(R.string.individual))) {
            showOnlyPaymentMethod();
        } else if (adapterView.getSelectedItemPosition() == 1 || adapterView.getSelectedItem().equals(getString(R.string.health_plan))) {
            showOnlyHealthPlan();
        }
    }

    @UiThread
    void showOnlyPaymentMethod() {
        layoutHealthPlan.setVisibility(View.GONE);
        layoutPaymentMethod.setVisibility(View.VISIBLE);
    }

    @UiThread
    void showOnlyHealthPlan() {
        layoutPaymentMethod.setVisibility(View.GONE);
        layoutHealthPlan.setVisibility(View.VISIBLE);
    }


    @UiThread
    void showSpinnersByServiceType(AdapterView<?> adapterView) {

        if (adapterView.getSelectedItemPosition() == 0 || adapterView.getSelectedItem().equals(getString(R.string.medical_appointment))) {
            showOnlySpeciality();
        }

        if (adapterView.getSelectedItemPosition() == 1 || adapterView.getSelectedItem().equals(getString(R.string.exam))) {
            showOnlyExamType();
        }
    }

    void showOnlySpeciality() {
        layoutExam.setVisibility(View.GONE);
        layoutSpeciality.setVisibility(View.VISIBLE);
    }

    void showOnlyExamType() {
        layoutSpeciality.setVisibility(View.GONE);
        layoutExam.setVisibility(View.VISIBLE);
    }

    @Click(R.id.buttonNext)
    void clickButtonNext() {
        if (layoutSpeciality.getVisibility() == View.VISIBLE || layoutExam.getVisibility() == View.VISIBLE) {
            setIdSelectedItems();
        }
    }

    @UiThread
    void setIdSelectedItems() {
        attendenceTypeId = getIdFromSelectedItem(layoutPlace, spinnerPlace);
        healthPlanId = getIdFromSelectedItem(layoutHealthPlan, spinnerHealthPlan);
        healthPlanName = getHealthPlanName(layoutHealthPlan, spinnerHealthPlan);
        paymentMethodId = getIdFromSelectedItem(layoutPaymentMethod, spinnerPaymentMethod);
        examTypeId = getIdFromSelectedExamType(layoutExam, spinnerExamType);
        specialityId = getIdFromSelectedSpeciality(layoutSpeciality, spinnerSpeciality);
        serviceTypeId = getIdFromSelectedItem(layoutServiceType, spinnerServiceType);

        setArguments();
    }

    public String getIdFromSelectedItem(LinearLayout layout, Spinner spinner) {

        if (layout.getVisibility() == View.VISIBLE) {
            return String.valueOf(spinner.getSelectedItemPosition() + 1 );
        } else {
            return "";
        }
    }

    public String getHealthPlanName(LinearLayout layout, Spinner spinner) {

        if (layout.getVisibility() == View.VISIBLE) {
            return spinner.getSelectedItem().toString();
        } else {
            return "";
        }
    }

    public String getIdFromSelectedSpeciality(LinearLayout layout, Spinner spinner) {

        if (layout.getVisibility() == View.VISIBLE) {
            return specialityService.getSpecialityWhereName(spinner.getSelectedItem().toString()).getId().toString();
        } else {
            return "";
        }
    }

    public String getIdFromSelectedExamType(LinearLayout layout, Spinner spinner) {

        if (layout.getVisibility() == View.VISIBLE) {
            return examTypeService.getExamTypeWhereName(spinner.getSelectedItem().toString()).getId().toString();
        } else {
            return "";
        }
    }

    @UiThread
    void setArguments() {
        Bundle data = new Bundle();
        dataPut(data, "attendenceTypeId", attendenceTypeId);
        dataPut(data, "healthPlanId", healthPlanId);
        dataPut(data, "healthPlanName", healthPlanName);
        dataPut(data, "paymentMethodId", paymentMethodId);
        dataPut(data, "examTypeId", examTypeId);
        dataPut(data, "specialityId", specialityId);
        dataPut(data, "serviceTypeId", serviceTypeId);

        callMapFragmentPatient(data);
    }

    @SuppressLint("NewApi")
    @UiThread
    void dataPut(Bundle data, String key, String value) {
        if (value != null && !Objects.equals(value, "")) {
            data.putString(key, value);
        }
    }

    @UiThread
    void callMapFragmentPatient(Bundle data) {
        Fragment fragment = new MapsFragmentPatient_();
        fragment.setArguments(data);
        switchFragment(fragment);
    }

    @UiThread
    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment);
        transaction.commit();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) { }

    @UiThread
    void startProgress() {
        if(getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if(getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }
}
