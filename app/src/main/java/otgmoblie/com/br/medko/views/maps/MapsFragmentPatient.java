package otgmoblie.com.br.medko.views.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.SearchParameters;
import otgmoblie.com.br.medko.service.ServiceWindowsService;
import otgmoblie.com.br.medko.util.DialogUtil;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.views.attendence.AttendenceActivity_;
import otgmoblie.com.br.medko.views.attendence.NewAttendenceFragment_;
import otgmoblie.com.br.medko.views.servicewindows.ServiceWindowsListFragment_;

@EFragment(R.layout.fragment_maps_patient)
public class MapsFragmentPatient extends Fragment implements OnMapReadyCallback {

    @ViewById(R.id.editTextAddressMaps)
    EditText editTextAddress;

    @ViewById(R.id.layoutLocatioButton)
    LinearLayout linearLayoutLocation;

    @Bean
    ServiceWindowsService serviceWindowsService;

    private List<android.location.Address> listAddress = null;

    private ProgressDialog progress;

    private GoogleMap googleMap;

    private Geocoder geocoder;

    private String address;

    private Bundle bundle;

    private LatLng center;

    private String attendenceTypeId, healthPlanId, healthPlanName, paymentMethodId, specialityId, examTypeId;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5;


    @AfterViews
    void afterViews() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        checkPermissionOnReload();
        bundle = getArguments();
        getIdsFromArguments();

    }

    @UiThread
    void getIdsFromArguments() {
        if (bundle != null) {
            attendenceTypeId = bundle.getString("attendenceTypeId");
            healthPlanId = bundle.getString("healthPlanId");
            healthPlanName = bundle.getString("healthPlanName");
            paymentMethodId = bundle.getString("paymentMethodId");
            specialityId = bundle.getString("specialityId");
            examTypeId = bundle.getString("examTypeId");
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng fortaleza = new LatLng(-3.732762, -38.526829);
        googleMap = map;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fortaleza, 14));
        listenCameraMovement();
    }

    @UiThread
    void listenCameraMovement() {
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                center = googleMap.getCameraPosition().target;
                setListAddress();
            }
        });
    }

    @UiThread
    void setListAddress() {
        if (center != null) {
            try {
                listAddress = geocoder.getFromLocation(center.latitude, center.longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            setAddress();
        }
    }

    @SuppressLint("SetTextI18n")
    void setAddress() {
        if (listAddress != null && listAddress.size() > 0) {
            address = listAddress.get(0).getAddressLine(0) + " - " + listAddress.get(0).getLocality() + " - " +
                    listAddress.get(0).getAdminArea() + " - " + listAddress.get(0).getCountryName();

        }

    }

    @UiThread
    void checkIfGpsIsEnabled() {
        final LocationManager gpsIsEnabled = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!gpsIsEnabled.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            checkPermissionToUseLocation();
        }
    }

    @Click(R.id.searchButton)
    void searchAddress() {
        if (editTextAddress.getText() != null && editTextAddress.getText().length() > 0) {

            try {
                listAddress = geocoder.getFromLocationName(editTextAddress.getText().toString(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            goToEnteredLocation();
        }
    }

    @SuppressLint("SetTextI18n")
    @UiThread
    void goToEnteredLocation() {
        if (listAddress != null && listAddress.size() > 0) {
            LatLng place = new LatLng(listAddress.get(0).getLatitude(), listAddress.get(0).getLongitude());
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place, 19));
            hideKeyboardFrom(getActivity(), editTextAddress);
        }
    }

    @Click(R.id.myLocationButton)
    void clickLocationButton() {
        checkIfGpsIsEnabled();
    }

    @Click(R.id.buttonNextMap)
    void clickButtonNext() {
        if(center != null) {
            setParams(new SearchParameters());
        }
    }

    private void setParams(SearchParameters searchParameters){
        searchParameters.setAddress(address);
        searchParameters.setLatitude(String.valueOf(center.latitude));
        searchParameters.setLongitude(String.valueOf(center.longitude));
        searchParameters.setAttendenceType(getParam(attendenceTypeId));
        searchParameters.setHealthPlan(getParam(healthPlanId));
        searchParameters.setPaymentMethod(getParam(paymentMethodId));
        searchParameters.setSpeciality(getParam(specialityId));
        searchParameters.setExamType(getParam(examTypeId));

        searchAttendence(searchParameters);
    }

    @SuppressLint("NewApi")
    @Background
    void searchAttendence(SearchParameters searchParameters) {

        if (Objects.equals(attendenceTypeId, "1")) {
            startProgress();
            try {
                Attendence newAttendence = serviceWindowsService.getServiceWindowsHomeCare(searchParameters);
                closeProgress();
                callAttendenceActivity(newAttendence);
            } catch (Exception e) {
                e.printStackTrace();
                closeProgress();
                callDialogAttendenceNotFound();
            }
        } else {
            if (center != null) {

                bundle.putString("latitude", String.valueOf(center.latitude));
                bundle.putString("longitude", String.valueOf(center.longitude));

                Fragment fragment = new ServiceWindowsListFragment_();
                fragment.setArguments(bundle);
                switchFragment(fragment);
            }
        }
    }

    @UiThread
    void callDialogAttendenceNotFound() {
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.attendece_not_found));
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        switchFragment(new NewAttendenceFragment_());
                    }
                });
        builder.show();

    }

    @UiThread
    void callAttendenceActivity(Attendence newAttendence) {
        if (newAttendence != null) {
            AttendenceActivity_.intent(getActivity()).attendenceId(newAttendence.getId()).start();
            getActivity().finish();
        }
    }

    @SuppressLint("NewApi")
    private String getParam(String id) {
        if (id != null && !Objects.equals(id, "")) {
            return id;
        } else {
            return "";
        }
    }

    @UiThread
    void checkPermissionToUseLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            linearLayoutLocation.setVisibility(View.VISIBLE);
        } else {
            linearLayoutLocation.setVisibility(View.GONE);
            googleMap.setMyLocationEnabled(true);
        }
    }

    @UiThread
    void checkPermissionOnReload() {
        int checkPermission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (checkPermission == PackageManager.PERMISSION_GRANTED) {
            checkIfGpsIsEnabled();
        }
    }

    @UiThread
    void buildAlertMessageNoGps() {
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.please_turn_on_your_gps));
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
            }
        });
        builder.show();
    }

    @UiThread
    void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @UiThread
    public void switchFragment(android.support.v4.app.Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.home_container, fragment);
        fragmentTransaction.commit();
    }

    @UiThread
    void startProgress() {
        if(getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if(getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }


}
