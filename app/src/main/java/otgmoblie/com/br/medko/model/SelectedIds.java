package otgmoblie.com.br.medko.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Thialyson on 13/12/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SelectedIds {

    @JsonProperty("id")
    private Integer serviceWindowsId;

    @JsonProperty("health")
    private Integer healthPlanId;

    @JsonProperty("payment")
    private Integer PaymentMethodId;


    public Integer getServiceWindowsId() {
        return serviceWindowsId;
    }

    public void setServiceWindowsId(Integer serviceWindowsId) {
        this.serviceWindowsId = serviceWindowsId;
    }

    public Integer getHealthPlanId() {
        return healthPlanId;
    }

    public void setHealthPlanId(Integer healthPlanId) {
        this.healthPlanId = healthPlanId;
    }

    public Integer getPaymentMethodId() {
        return PaymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        PaymentMethodId = paymentMethodId;
    }
}
