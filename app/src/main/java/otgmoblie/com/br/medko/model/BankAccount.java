package otgmoblie.com.br.medko.model;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by augustuscosta on 13/07/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class BankAccount{

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String bank;

    @DatabaseField
    @JsonProperty("full_name")
    private String fullName;

    @DatabaseField
    @JsonProperty("cpf_cnpj")
    private String cpfCnpj;

    @DatabaseField
    @JsonProperty
    private String account;

    @DatabaseField
    @JsonProperty
    private String agency;

    @DatabaseField
    @JsonProperty("operation")
    private String operation;

    @JsonProperty
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAgency() { return agency; }

    public void setAgency(String agency) { this.agency = agency; }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
