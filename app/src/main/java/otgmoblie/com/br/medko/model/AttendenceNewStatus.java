package otgmoblie.com.br.medko.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Thialyson on 29/09/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttendenceNewStatus {

    @JsonProperty
    private Integer id;

    @JsonProperty()
    private String date;


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }

}
