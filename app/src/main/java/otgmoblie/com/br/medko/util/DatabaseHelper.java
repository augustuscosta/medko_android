package otgmoblie.com.br.medko.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.AttendenceStatus;
import otgmoblie.com.br.medko.model.AttendenceType;
import otgmoblie.com.br.medko.model.BankAccount;
import otgmoblie.com.br.medko.model.Classification;
import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.model.HealthPlan;
import otgmoblie.com.br.medko.model.Payment;
import otgmoblie.com.br.medko.model.PaymentMethod;
import otgmoblie.com.br.medko.model.Place;
import otgmoblie.com.br.medko.model.SearchAddress;
import otgmoblie.com.br.medko.model.Session;
import otgmoblie.com.br.medko.model.Speciality;
import otgmoblie.com.br.medko.model.TakeTime;
import otgmoblie.com.br.medko.model.User;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "medko.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Session.class);
            TableUtils.createTable(connectionSource, BankAccount.class);
            TableUtils.createTable(connectionSource, Speciality.class);
            TableUtils.createTable(connectionSource, ExamType.class);
            TableUtils.createTable(connectionSource, Attendence.class);
            TableUtils.createTable(connectionSource, AttendenceType.class);
            TableUtils.createTable(connectionSource, AttendenceStatus.class);
            TableUtils.createTable(connectionSource, Classification.class);
            TableUtils.createTable(connectionSource, HealthPlan.class);
            TableUtils.createTable(connectionSource, TakeTime.class);
            TableUtils.createTable(connectionSource, Place.class);
            TableUtils.createTable(connectionSource, SearchAddress.class);
            TableUtils.createTable(connectionSource, Payment.class);
            TableUtils.createTable(connectionSource, PaymentMethod.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {

            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, Session.class, true);
            TableUtils.dropTable(connectionSource, BankAccount.class, true);
            TableUtils.dropTable(connectionSource, Speciality.class, true);
            TableUtils.dropTable(connectionSource, ExamType.class, true);
            TableUtils.dropTable(connectionSource, Attendence.class, true);
            TableUtils.dropTable(connectionSource, AttendenceType.class, true);
            TableUtils.dropTable(connectionSource, AttendenceStatus.class, true);
            TableUtils.dropTable(connectionSource, Classification.class, true);
            TableUtils.dropTable(connectionSource, HealthPlan.class, true);
            TableUtils.dropTable(connectionSource, TakeTime.class, true);
            TableUtils.dropTable(connectionSource, Place.class, true);
            TableUtils.dropTable(connectionSource, SearchAddress.class, true);
            TableUtils.dropTable(connectionSource, Payment.class, true);
            TableUtils.dropTable(connectionSource, PaymentMethod.class, true);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        super.close();
    }
}
