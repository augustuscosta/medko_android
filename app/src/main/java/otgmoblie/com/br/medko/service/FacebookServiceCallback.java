package otgmoblie.com.br.medko.service;

import otgmoblie.com.br.medko.model.User;

/**
 * Created by augustuscosta on 14/07/16.
 */
public interface FacebookServiceCallback {
    void onFacebookLoginSuccess(User user);
    void onFacebookLoginError(String message);
}
