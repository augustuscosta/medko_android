package otgmoblie.com.br.medko.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.AttendenceNewStatus;
import otgmoblie.com.br.medko.model.Classification;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by augustuscosta on 22/06/16.
 */

@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface AttendenceRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("attendences.json")
    List<Attendence> getAll();

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("attendences/{id}.json")
    Attendence getAttendence(@Path Integer id);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("attendences/time.json")
    void setAttendenceStatusWithTime(@Body AttendenceNewStatus attendenceNewStatus);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("attendences/confirm.json")
    void setAttendenceStatusToConfirm(@Body AttendenceNewStatus attendenceNewStatus);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("attendences/cancel.json")
    void setAttendenceStatusToCancel(@Body AttendenceNewStatus attendenceNewStatus);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("attendences/payment.json")
    void setAttendenceStatusToMarked(@Body AttendenceNewStatus attendenceNewStatus);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("classification.json")
    void setClassification(@Body Classification classification);

    String getCookie(String name);

    void setCookie(String name, String value);

}
