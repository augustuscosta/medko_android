package otgmoblie.com.br.medko.model;

/**
 * Created by Thialyson on 22/08/16.
 */
public class Point {

    private Double latitude;
    private Double longitude;

    public Point() {
        this.latitude = 0.0;
        this.longitude = 0.0;
    }

    public Point(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
