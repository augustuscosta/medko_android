package otgmoblie.com.br.medko.views.servicewindows;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.ServiceWindows;


/**
 * Created by thialyson on 18/10/16.
 */

class ServiceWindowsListAdapter extends RecyclerView.Adapter<ServiceWindowsListAdapter.ViewHolder> {

    private List<ServiceWindows> serviceWindowsList;
    private LayoutInflater layoutInflater;
    private String serviceTypeId, healthPlanId, healthPlanName;
    private AdapterView.OnItemClickListener onItemClickListener;


    ServiceWindowsListAdapter(List<ServiceWindows> serviceWindowsList, Context context, String serviceTypeId, String healthPlanId, String healthPlanName, AdapterView.OnItemClickListener onItemClickListener) {
        this.serviceWindowsList = serviceWindowsList;
        this.serviceTypeId = serviceTypeId;
        this.healthPlanId = healthPlanId;
        this.healthPlanName = healthPlanName;
        this.onItemClickListener = onItemClickListener;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_service_windows, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ServiceWindows item = serviceWindowsList.get(position);

        if (serviceWindowsList != null) {
            holder.imageUser.setImageResource(R.drawable.ic_doctor);
            holder.textViewNameUser.setText(item.getDoctor().getName());
            holder.textViewNameUser.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            holder.namePlaceServiceWindows.setText(item.getPlace().getName());
            holder.textViewPlaceServiceWindows.setText(item.getPlace().getAddress());
            holder.textViewDateTimeServiceWindows.setText(item.getTakeTime());
            showPrice(holder, item);
            showExamType(holder, item);
            showSpeciality(holder, item);
            showRatingBar(holder, item);
        }

    }

    @SuppressLint("DefaultLocale")
    private void showPrice(ViewHolder holder, ServiceWindows item) {
        if (item.getHealthPlan().size() > 0 && healthPlanName != null) {
            holder.textViewDollarSign.setVisibility(View.GONE);
            holder.textViewPrice.setText(healthPlanName);

        }else {
            holder.textViewDollarSign.setVisibility(View.VISIBLE);
            holder.textViewPrice.setText(String.format("%.2f", item.getValue()).replace(".", ","));
        }
    }


    private void showSpeciality(ViewHolder holder, ServiceWindows item) {
        if (serviceTypeId != null && serviceTypeId.equals("1")) {
            holder.textViewExam.setVisibility(View.GONE);
            holder.textViewSpeciality.setVisibility(View.VISIBLE);
            holder.textViewSpeciality.setText(item.getSpeciality().getName());
        }
    }

    private void showExamType(ViewHolder holder, ServiceWindows item) {
        if (serviceTypeId != null && serviceTypeId.equals("2")) {
            holder.textViewSpeciality.setVisibility(View.GONE);
            holder.textViewExam.setVisibility(View.VISIBLE);
            holder.textViewExam.setText(item.getExamType().getName());
        }
    }

    private void showRatingBar(ViewHolder holder, ServiceWindows item) {
        if (item.getDoctor().getRating() && item.getDoctor().getMedia() > 0) {
            holder.ratingBar.setVisibility(View.VISIBLE);
            holder.ratingBar.setRating(item.getDoctor().getMedia());
        }
    }


    @Override
    public int getItemCount() {
        if (serviceWindowsList != null) {
            return serviceWindowsList.size();
        } else {
            return 0;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RatingBar ratingBar;
        CardView cardView;
        ImageView imageUser;
        TextView textViewNameUser, textViewExam, textViewSpeciality, textViewPrice, textViewDollarSign, textViewPlaceServiceWindows, textViewDateTimeServiceWindows, namePlaceServiceWindows;

        ViewHolder(final View itemView) {
            super(itemView);


            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBarServiceWindows);
            imageUser = (ImageView) itemView.findViewById(R.id.imageUserServiceWindows);
            textViewNameUser = (TextView) itemView.findViewById(R.id.textViewNameUserServiceWindows);
            textViewNameUser.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            textViewExam = (TextView) itemView.findViewById(R.id.textViewExamServiceWindows);
            textViewSpeciality = (TextView) itemView.findViewById(R.id.textViewSpecialityServiceWindows);
            textViewPrice = (TextView) itemView.findViewById(R.id.textViewPrice);
            textViewDollarSign = (TextView) itemView.findViewById(R.id.textViewDolarSignServiceWindows);
            textViewPlaceServiceWindows = (TextView) itemView.findViewById(R.id.textViewPlaceServiceWindows);
            textViewDateTimeServiceWindows = (TextView) itemView.findViewById(R.id.textViewDateTimeServiceWindows);
            namePlaceServiceWindows = (TextView) itemView.findViewById(R.id.textViewNamePlaceServiceWindows);
            cardView = (CardView) itemView.findViewById(R.id.cardViewServiceWindows);

            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());
        }
    }
}
