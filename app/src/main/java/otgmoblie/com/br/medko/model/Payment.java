package otgmoblie.com.br.medko.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

/**
 * Created by Thialyson on 09/09/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Payment {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private Integer value;

    @ForeignCollectionField
    @JsonProperty("attendence")
    private Collection<Attendence> attendences;


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getValue() { return value; }

    public void setValue(Integer value) { this.value = value; }

    public Collection<Attendence> getAttendences() { return attendences; }

    public void setAttendences(Collection<Attendence> attendences) { this.attendences = attendences; }

}
