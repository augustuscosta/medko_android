package otgmoblie.com.br.medko.util;

/**
 * Created by Thialyson on 23/09/16.
 */
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

public final class IntentUtil {
    public static boolean isIntentAvailable(Context context, Intent intent) {
        if (context == null || intent == null) {
            return false;
        }

        List<ResolveInfo> resolveInfo = context.getPackageManager()
                .queryIntentActivities(intent, PackageManager.GET_ACTIVITIES);
        if (resolveInfo == null || resolveInfo.isEmpty()) {
            return false;
        }

        return true;
    }

}