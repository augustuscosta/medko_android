package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import otgmoblie.com.br.medko.cloud.RegistrationMultipartRest;
import otgmoblie.com.br.medko.cloud.RegistrationRest;
import otgmoblie.com.br.medko.model.BankAccount;
import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.model.Session;
import otgmoblie.com.br.medko.model.Speciality;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.model.UserWrapper;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by augustuscosta on 23/06/16.
 */
@EBean
public class RegistrationService {

    @RestService
    RegistrationRest registrationRest;

    @RestService
    RegistrationMultipartRest registrationMultipartRest;

    @RootContext
    Context context;

    @Bean(CookieService.class)
    CookieService cookieUtils;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Session, Integer> sessionDao;


    public boolean register(User user) {
        boolean returnValue;
        try {

            UserWrapper wrapper = new UserWrapper();
            wrapper.setUser(user);
            registrationRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            persist(registrationRest.register(wrapper));
            cookieUtils.storedCookie(registrationRest.getCookie(RestUtil.MOBILE_COOKIE));


            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }

    private void persist(Session session) {
        try {
            sessionDao.createOrUpdate(session);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public Session getCurrentSession() {
        try {
            List<Session> sessions = sessionDao.queryForAll();
            if (sessions.size() > 0)
                return sessions.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(sessionDao.getConnectionSource(), Session.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User register(User user, FileSystemResource imageAvatar, FileSystemResource imageCrm, FileSystemResource imageProofOfAddress) {
        User returnedUser;

        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
        HttpHeaders imageHeaders = new HttpHeaders();
        imageHeaders.setContentType(MediaType.IMAGE_JPEG);

        if (imageAvatar != null) {
            HttpEntity<Resource> imageEntity = new HttpEntity(imageAvatar, imageHeaders);
            addValue(data, "user[avatar]", imageEntity);
        }

        if (imageCrm != null) {
            HttpEntity<Resource> imageEntityCrm = new HttpEntity(imageCrm, imageHeaders);
            addValue(data, "user[crm_image]", imageEntityCrm);

        }
        if (imageProofOfAddress != null) {
            HttpEntity<Resource> imageEntityProofOfAddress = new HttpEntity(imageProofOfAddress, imageHeaders);
            addValue(data, "user[proof_of_address_image]", imageEntityProofOfAddress);
        }

        getData(user, data);
        getSpecialitiesData(user, data);
        getExamTypesData(user, data);
        getBankAccountData(user, data);

        try {
            registrationMultipartRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            registrationMultipartRest.setHeader("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
            returnedUser = registrationMultipartRest.uploadImage(data);
            cookieUtils.storedCookie(registrationMultipartRest.getCookie(RestUtil.MOBILE_COOKIE));
            return returnedUser;
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }

    }

    private void getData(User user, MultiValueMap<String, Object> data) {
        addValue(data, "user[name]", user.getName());
        addValue(data, "user[crm]", user.getCrm());
        addValue(data, "user[email]", user.getEmail());
        addValue(data, "user[value_home_care]", user.getValueHomeCare());
        addValue(data, "user[password]", user.getPassword());
        addValue(data, "user[password_confirmation]", user.getPasswordConfirmation());
        addValue(data, "user[telephone]", user.getTelephone());
        addValue(data, "user[zip_code]", user.getZipCode());
        addValue(data, "user[address]", user.getAddress());
        addValueAsString(data, "user[account_type_id]", user.getAccountTypeId());
        addValueAsString(data, "user[active]", user.getActive());
        addValueAsString(data, "user[rating]", user.getRating());
    }

    private void getExamTypesData(User user, MultiValueMap<String, Object> data) {
        if (user.getExamTypes() == null || user.getExamTypes().size() == 0)
            return;
        List<ExamType> listExamType = new ArrayList<>(user.getExamTypes());
        List<String> listExamTypeIds = new ArrayList<>();
        for (int i = 0; i < listExamType.size(); i++) {
            listExamTypeIds.add(listExamType.get(i).getId().toString());
        }
        for (int i = 0; i < listExamTypeIds.size(); i++) {
            addValuesToArray(data, "user[exam_type_ids][]", listExamTypeIds.get(i));
        }
    }

    private void getSpecialitiesData(User user, MultiValueMap<String, Object> data) {
        if (user.getSpecialities() == null || user.getSpecialities().size() == 0)
            return;
        List<Speciality> listSpeciality = new ArrayList<>(user.getSpecialities());
        List<String> listSpecialityIds = new ArrayList<>();
        for (int i = 0; i < listSpeciality.size(); i++) {
            listSpecialityIds.add(listSpeciality.get(i).getId().toString());
        }
        for (int i = 0; i < listSpecialityIds.size(); i++) {
            addValuesToArray(data, "user[speciality_ids][]", listSpecialityIds.get(i));
        }
    }

    private void getBankAccountData(User user, MultiValueMap<String, Object> data) {
        if (user.getBankAccounts() == null || user.getBankAccounts().size() == 0)
            return;
        List<BankAccount> listBank = new ArrayList<>(user.getBankAccounts());
        addValue(data, "user[bank_account_attributes][0][full_name]", (listBank.get(0).getFullName()));
        addValue(data, "user[bank_account_attributes][0][cpf_cnpj]", (listBank.get(0).getCpfCnpj()));
        addValue(data, "user[bank_account_attributes][0][bank]", (listBank.get(0).getBank()));
        addValue(data, "user[bank_account_attributes][0][account]", (listBank.get(0).getAccount()));
        addValue(data, "user[bank_account_attributes][0][agency]", (listBank.get(0).getAgency()));
        addValue(data, "user[bank_account_attributes][0][operation]", (listBank.get(0).getOperation()));
    }

    private void addValue(MultiValueMap data, String key, Object value) {
        if (value == null) {
            return;
        } else {
            data.set(key, value);
        }
    }

    private void addValueAsString(MultiValueMap data, String key, Object value) {
        if (value == null) {
            return;
        } else {
            data.set(key, value + "");
        }
    }

    private void addValuesToArray(MultiValueMap data, String key, Object value) {
        if (value == null) {
            return;
        } else {
            data.add(key, value + "");
        }
    }

}
