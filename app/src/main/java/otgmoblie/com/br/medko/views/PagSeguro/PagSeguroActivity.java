package otgmoblie.com.br.medko.views.PagSeguro;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import otgmoblie.com.br.medko.R;

@EActivity(R.layout.activity_pag_seguro)
public class PagSeguroActivity extends AppCompatActivity {

    @Extra
    String url;

    @ViewById
    WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    @AfterViews
    void afterViews(){
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);

        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            @SuppressWarnings("deprecation") // Na verdade esse é o método suportado por todas as versões do Android o novo método so na N
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                Log.d("URL Redirect", url);
                checkUrl(url);
                return false; //Allow WebView to load url
            }
        });
    }

    void checkUrl(String url){
        if(url.contains("/attendences/")){
            setResultAndFinish();
        }
    }

    public void onBackPressed() {
        finish();
    }


    void setResultAndFinish(){
        setResult(RESULT_OK);
        finish();
    }
}
