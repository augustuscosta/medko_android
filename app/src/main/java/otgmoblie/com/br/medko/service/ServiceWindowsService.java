package otgmoblie.com.br.medko.service;

import android.content.Context;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;

import otgmoblie.com.br.medko.cloud.ServiceWindowsRest;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.SearchParameters;
import otgmoblie.com.br.medko.model.SelectedIds;
import otgmoblie.com.br.medko.model.ServiceWindows;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 17/10/16.
 */
@EBean
public class ServiceWindowsService {

    @RootContext
    Context context;

    @RestService
    ServiceWindowsRest serviceWindowsRest;

    @Bean
    CookieService cookieService;


    public List<ServiceWindows> getServiceWindowsList(SearchParameters searchParameters) throws Exception {

        List<ServiceWindows> toReturn = null;

        try {
            serviceWindowsRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = new ArrayList<>(serviceWindowsRest.getServiceWindowsList(searchParameters));
            System.out.println();
            cookieService.storedCookie(serviceWindowsRest.getCookie(RestUtil.MOBILE_COOKIE));

        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    public Attendence getServiceWindowsHomeCare(SearchParameters searchParameters) throws Exception {

        Attendence toReturn = null;

        try {
            serviceWindowsRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = serviceWindowsRest.getServiceWindowsHomeCare(searchParameters);
            cookieService.storedCookie(serviceWindowsRest.getCookie(RestUtil.MOBILE_COOKIE));

        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    public Attendence getServiceWindowsClinic(SelectedIds selectedIds) throws Exception {

        Attendence toReturn = null;

        try {
            serviceWindowsRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = serviceWindowsRest.sendServiceWindowId(selectedIds);
            cookieService.storedCookie(serviceWindowsRest.getCookie(RestUtil.MOBILE_COOKIE));

        }catch (RestClientException e) {
            e.printStackTrace();
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

}
