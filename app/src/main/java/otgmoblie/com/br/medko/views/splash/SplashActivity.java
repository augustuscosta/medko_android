package otgmoblie.com.br.medko.views.splash;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.views.login.LoginActivity_;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @AfterViews
    void afterViews(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                LoginActivity_.intent(SplashActivity.this).start();
            }
        }, 4000);
    }

    @Override
    public void onBackPressed() { }
}
