package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import otgmoblie.com.br.medko.cloud.ExamTypeRest;
import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 28/07/16.
 */

@EBean
public class ExamTypeService {

    @RootContext
    Context context;

    @RestService
    ExamTypeRest examTypeRest;

    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<ExamType, Integer> examTypeDao;


    public List<ExamType> getAll() {

        try {
            return examTypeDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<ExamType> getAllExamTypesByName() {

        try {
            return examTypeDao.queryBuilder().orderBy("name", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public ExamType getExamTypeWhereName(String nome) {

        ExamType examTypeTemp = new ExamType();
        try {
            QueryBuilder<ExamType, Integer> queryBuilder = examTypeDao.queryBuilder();
            examTypeTemp = queryBuilder.where().eq("name", nome).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return examTypeTemp;
    }

    public List<ExamType> request() throws Exception {
        List<ExamType> toReturn = null;

        try {
            examTypeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = examTypeRest.getAll();
            clearTable();
            persist(toReturn);
            cookieService.storedCookie(examTypeRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    private void persist(ExamType examType) {
        try {
            examTypeDao.createOrUpdate(examType);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void persist(List<ExamType> examTypes) {
        if (examTypes == null)
            return;
        for (ExamType examType : examTypes) {
            persist(examType);
        }
    }

    public ExamType get(Integer id) {
        try {
            return examTypeDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(examTypeDao.getConnectionSource(), ExamType.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
