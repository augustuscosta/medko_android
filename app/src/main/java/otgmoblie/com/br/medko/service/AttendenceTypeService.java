package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import otgmoblie.com.br.medko.cloud.AttendenceTypeRest;
import otgmoblie.com.br.medko.model.AttendenceType;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 07/10/16.
 */
@EBean
public class AttendenceTypeService {

    @RootContext
    Context context;

    @RestService
    AttendenceTypeRest attendenceTypeRest;

    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<AttendenceType, Integer> attendenceTypeDao;

    public List<AttendenceType> getAll() {

        try{
            return attendenceTypeDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<AttendenceType> request() throws Exception {

        List<AttendenceType> toReturn = null;

        try {
            attendenceTypeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = attendenceTypeRest.getAll();
            clearTable();
            persist(toReturn);
            cookieService.storedCookie(attendenceTypeRest.getCookie(RestUtil.MOBILE_COOKIE));

        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    private void persist(AttendenceType attendenceType){
        try{
            attendenceTypeDao.createOrUpdate(attendenceType);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void persist(List<AttendenceType> attendenceTypes){
        if(attendenceTypes == null)
            return;

        for(AttendenceType attendenceType:attendenceTypes){
            persist(attendenceType);
        }
    }

    public AttendenceType get(Integer id) {
        try{
            return attendenceTypeDao.queryForId(id);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(attendenceTypeDao.getConnectionSource(), AttendenceType.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
