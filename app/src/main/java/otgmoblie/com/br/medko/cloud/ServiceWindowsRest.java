package otgmoblie.com.br.medko.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.SearchParameters;
import otgmoblie.com.br.medko.model.SelectedIds;
import otgmoblie.com.br.medko.model.ServiceWindows;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 17/10/16.
 */
@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface ServiceWindowsRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("attendences/search_attendence_clinic.json")
    List<ServiceWindows> getServiceWindowsList(@Body SearchParameters searchParameters);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("attendences/create_attendence.json")
    Attendence sendServiceWindowId(@Body SelectedIds selectedIds);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("attendences/search_attendence_home_care.json")
    Attendence getServiceWindowsHomeCare(@Body SearchParameters searchParameters);


    String getCookie(String name);

    void setCookie(String name, String value);
}
