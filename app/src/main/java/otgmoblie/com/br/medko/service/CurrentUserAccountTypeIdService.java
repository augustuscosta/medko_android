package otgmoblie.com.br.medko.service;

import android.content.Context;
import android.content.SharedPreferences;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Thialyson on 22/09/16.
 */
@EBean
public class CurrentUserAccountTypeIdService {
    @RootContext
    Context context;

    private void saveSharedPreferencesCurrentUserId(Integer valueId, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("PREFERENCES_CURRENT_USER_ID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("PREFERENCES_CURRENT_USER_ID", valueId);
        editor.apply();
    }

    public int getSharedPreferencesCurrentUserId(Context context) {
        if(context != null) {
            SharedPreferences sharedPref = context.getSharedPreferences("PREFERENCES_CURRENT_USER_ID", Context.MODE_PRIVATE);
            return sharedPref.getInt("PREFERENCES_CURRENT_USER_ID", 0);
        }else{
            return 0;
        }
    }

    public void updateSharedPreferencesCurrentUserId(int valueId) {
        saveSharedPreferencesCurrentUserId(valueId, context);
    }
}
