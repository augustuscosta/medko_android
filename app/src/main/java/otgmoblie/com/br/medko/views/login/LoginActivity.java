package otgmoblie.com.br.medko.views.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.service.DeviceIdService;
import otgmoblie.com.br.medko.service.SessionService;
import otgmoblie.com.br.medko.service.UserService;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.views.home.HomeActivity_;


@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @ViewById(R.id.tabLayout)
    TabLayout tabLayout;

    @ViewById(R.id.viewPager)
    ViewPager viewPager;

    @ViewById(R.id.barLayout)
    AppBarLayout appBarLayout;

    @ViewById(R.id.toolBar)
    Toolbar toolbar;

    @Bean
    DeviceIdService deviceIdService;

    @Bean
    SessionService sessionService;

    @Bean
    UserService userService;

    ViewPagerAdapter viewPagerAdapter;

    private LoginPatientFragment loginPatientFragment;

    boolean doubleBackToExitPressedOnce = false;

    private ProgressDialog progress;

    @AfterViews
    void afterViews() {

        if (sessionService.getCurrentSession() != null) {
            startProgress();
            callHomeActivity();
            closeProgress();

        } else {
            loginPatientFragment = new LoginPatientFragment_();

            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPagerAdapter.addFragments(loginPatientFragment, getString(R.string.title_fragment_login_patient));
            viewPagerAdapter.addFragments(new LoginDoctorFragment_(), getString(R.string.title_fragment_login_doctor));
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        }

    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
        progress.show();
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginPatientFragment.facebookService.callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_again_to_go_out, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @UiThread
    void callHomeActivity() {
        HomeActivity_.intent(this).start();
        finish();
    }

}
