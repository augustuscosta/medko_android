package otgmoblie.com.br.medko.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 28/07/16.
 */

@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface ExamTypeRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("/exam_types.json")
    List<ExamType> getAll();

    String getCookie(String name);

    void setCookie(String name, String value);
}
