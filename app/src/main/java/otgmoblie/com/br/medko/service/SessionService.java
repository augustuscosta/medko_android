package otgmoblie.com.br.medko.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.List;

import otgmoblie.com.br.medko.cloud.SessionRest;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.AttendenceStatus;
import otgmoblie.com.br.medko.model.AttendenceType;
import otgmoblie.com.br.medko.model.BankAccount;
import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.model.HealthPlan;
import otgmoblie.com.br.medko.model.Payment;
import otgmoblie.com.br.medko.model.PaymentMethod;
import otgmoblie.com.br.medko.model.Place;
import otgmoblie.com.br.medko.model.SearchAddress;
import otgmoblie.com.br.medko.model.Session;
import otgmoblie.com.br.medko.model.Speciality;
import otgmoblie.com.br.medko.model.TakeTime;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.model.UserWrapper;
import otgmoblie.com.br.medko.util.DatabaseHelper;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by augustuscosta on 23/06/16.
 */
@EBean
public class SessionService {

    @RestService
    SessionRest sessionRest;

    @RootContext
    Context context;

    @Bean(CookieService.class)
    CookieService cookieUtils;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Session, Integer> sessionDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<User, Integer> userDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<ExamType, Integer> examTypeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Speciality, Integer> specialityDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<BankAccount, Integer> bankAccountDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Attendence, Integer> attendenceDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Place, Integer> placeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<TakeTime, Integer> TakeTimeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<HealthPlan, Integer> HealtPlanDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Payment, Integer> PaymentDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<PaymentMethod, Integer> PaymentMethodDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<AttendenceType, Integer> AttendenceTypeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<AttendenceStatus, Integer> AttendenceStatusDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<SearchAddress, Integer> searchAddressDao;


    public void signIn(Session session) throws Exception {

        sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
        persist(sessionRest.signIn(session));
        cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));
    }

    public boolean signInFacebook(User user) throws Exception {
        boolean returnValue;
        try {

            UserWrapper wrapper = new UserWrapper();
            wrapper.setUser(user);
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            persist(sessionRest.signInFacebook(wrapper));
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));

            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }


    public boolean signOut() {

        try {
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            sessionRest.signOut();
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));
            clearTable();

            return true;

        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }

    }

    public boolean resetPassword(Session session) {
        boolean returnValue;
        try {
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            sessionRest.resetPassword(session);
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));

            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }

    private void persist(Session session) {
        try {
            sessionDao.createOrUpdate(session);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public Session getCurrentSession() {
        try {
            List<Session> sessions = sessionDao.queryForAll();
            if (sessions.size() > 0)
                return sessions.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void clearTable() {
        try {
            TableUtils.clearTable(sessionDao.getConnectionSource(), Session.class);
            TableUtils.clearTable(userDao.getConnectionSource(), User.class);
            TableUtils.clearTable(examTypeDao.getConnectionSource(), ExamType.class);
            TableUtils.clearTable(specialityDao.getConnectionSource(), Speciality.class);
            TableUtils.clearTable(bankAccountDao.getConnectionSource(), BankAccount.class);
            TableUtils.clearTable(attendenceDao.getConnectionSource(), Attendence.class);
            TableUtils.clearTable(placeDao.getConnectionSource(), Place.class);
            TableUtils.clearTable(searchAddressDao.getConnectionSource(), SearchAddress.class);
            TableUtils.clearTable(TakeTimeDao.getConnectionSource(), TakeTime.class);
            TableUtils.clearTable(HealtPlanDao.getConnectionSource(), HealthPlan.class);
            TableUtils.clearTable(PaymentDao.getConnectionSource(), Payment.class);
            TableUtils.clearTable(PaymentMethodDao.getConnectionSource(), PaymentMethod.class);
            TableUtils.clearTable(AttendenceTypeDao.getConnectionSource(), AttendenceType.class);
            TableUtils.clearTable(AttendenceStatusDao.getConnectionSource(), AttendenceStatus.class);
            FacebookService.saveSharedPreferencesTokenFacebook("", context);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
