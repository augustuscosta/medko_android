package otgmoblie.com.br.medko.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import otgmoblie.com.br.medko.model.Address;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 20/10/16.
 */
@Rest(rootUrl = "https://viacep.com.br/", converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface AddressRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("ws/{cep}/json/")
    Address getAddress(@Path String cep);

    String getCookie(String name);

    void setCookie(String name, String value);

}
