package otgmoblie.com.br.medko.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Thialyson on 09/09/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Classification {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty("attendence_id")
    private Integer attendenceId;

    @DatabaseField
    @JsonProperty
    private Integer value;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Attendence attendence;


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getAttendenceId() { return attendenceId; }

    public void setAttendenceId(Integer attendenceId) { this.attendenceId = attendenceId; }

    public Integer getValue() { return value; }

    public void setValue(Integer value) { this.value = value; }

    public Attendence getAttendence() { return attendence; }

    public void setAttendence(Attendence attendence) { this.attendence = attendence; }
}
