package otgmoblie.com.br.medko.views.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;
import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.service.CurrentUserAccountTypeIdService;
import otgmoblie.com.br.medko.service.SessionService;
import otgmoblie.com.br.medko.service.UserService;
import otgmoblie.com.br.medko.tasks.ServiceLocation_;
import otgmoblie.com.br.medko.util.DialogUtil;
import otgmoblie.com.br.medko.util.RestUtil;
import otgmoblie.com.br.medko.views.attendence.AttendenceListFragment_;
import otgmoblie.com.br.medko.views.attendence.NewAttendenceFragment_;
import otgmoblie.com.br.medko.views.login.LoginActivity_;
import otgmoblie.com.br.medko.views.maps.MapsFragmentDoctor_;

@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Bean
    SessionService sessionService;

    @Bean
    UserService userService;

    @Bean
    CurrentUserAccountTypeIdService currentUserAccountTypeIdService;

    private boolean doubleBackToExitPressedOnce = false;

    private CircleImageView circleImageView;

    private TextView nameUserLogged;

    private RatingBar ratingBar;

    private User userLogged;

    private View header;


    @AfterViews
    void afterViews() {
        requestCurrentUser();
    }

    @Background
    void requestCurrentUser() {
        try {
            userService.getCurrentUser();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        getFromDatabase();
    }

    @UiThread
    void getFromDatabase() {
        userLogged = userService.getLocalCurrentUser();
        setNavigationView();
        loadComponents();
    }

    @UiThread
    void setNavigationView() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        setHeadersComponents(header);
    }

    @UiThread
    void loadComponents() {

        if (userLogged != null) {
            currentUserAccountTypeIdService.updateSharedPreferencesCurrentUserId(userLogged.getAccountTypeId());

            chooseMenu();
            callPersonalFragment();
        }
    }

    @UiThread
    void setHeadersComponents(View header) {
        if (userLogged != null) {
            nameUserLogged = (TextView) header.findViewById(R.id.nameUserLoged);
            circleImageView = (de.hdodenhof.circleimageview.CircleImageView) header.findViewById(R.id.imageViewUser);
            ratingBar = (RatingBar) header.findViewById(R.id.ratingBar);
        }
        setAvatarCurrentUser();
    }


    void setAvatarCurrentUser() {
        if (userLogged != null) {
            try {
                String url = RestUtil.ROOT_URL_INDEX + userLogged.getAvatar();
                Picasso.with(this).load(url).resize(100, 100).centerCrop().error(R.drawable.ic_user).into(circleImageView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @UiThread
    void setAvatar(Bitmap bitmap) {
        if (bitmap != null) {
            circleImageView.setImageBitmap(bitmap);
        }
    }

    @UiThread
    void chooseMenu() {
        if (userLogged != null) {
            if (userLogged.getAccountTypeId() == 3) {
                getCommonMenu(R.menu.activity_home_drawer_doctor);
                setItemSelectedMenu(0);
                showRatingBar();

            } else {
                getCommonMenu(R.menu.activity_home_drawer_patient);
                setItemSelectedMenu(1);
            }
        }
    }

    @UiThread
    void getCommonMenu(Integer resId) {
        if (userLogged != null) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(resId);
            nameUserLogged.setText(userLogged.getName());
        }
    }

    @UiThread
    void setItemSelectedMenu(Integer index) {
        navigationView.getMenu().getItem(index).setChecked(true);
    }

    @UiThread
    void showRatingBar() {
        if (userLogged.getRating() != null && userLogged.getRating()) {
            ratingBar = (RatingBar) header.findViewById(R.id.ratingBar);
            ratingBar.setVisibility(View.VISIBLE);

            if (userLogged.getMedia() != null && userLogged.getMedia() > 0) {
                ratingBar.setRating(userLogged.getMedia());
            }
        }
    }

    @UiThread
    void callPersonalFragment() {
        if (currentUserAccountTypeIdService.getSharedPreferencesCurrentUserId(this) == 1) {
            switchFragment(new AttendenceListFragment_());
        } else {
            switchFragment(new MapsFragmentDoctor_());
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_frame1:
                switchFragment(new MapsFragmentDoctor_());
                break;
            case R.id.item_frame2:
                switchFragment(new AttendenceListFragment_());
                break;
            case R.id.item_frame3:
                dialogSignOut();
                break;
            case R.id.item_frame4:
                switchFragment(new NewAttendenceFragment_());
                break;
        }
        item.setChecked(true);
        drawerLayout.closeDrawers();
        return true;
    }

    @UiThread
    void dialogSignOut() {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(R.string.do_you_wish_go_out));
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        signOut();
                    }
                });
        builder.show();
    }

    @Background
    void signOut() {
        sessionService.signOut();
        HomeActivity.this.stopService(new Intent(HomeActivity.this, ServiceLocation_.class));
        callLoginActivity();
    }

    @UiThread
    void callLoginActivity() {
        LoginActivity_.intent(HomeActivity.this).start();
        finish();
    }

    @UiThread
    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_again_to_go_out, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Receiver(actions = "otgmobile.com.br.medko.UNAUTHORIZED")
    protected void checkUnauthorizedUser() {
        Toast.makeText(this, R.string.access_denied, Toast.LENGTH_SHORT).show();
        sessionService.clearTable();
        callLoginActivity();
    }
}
