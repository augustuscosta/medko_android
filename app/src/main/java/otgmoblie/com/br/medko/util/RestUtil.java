package otgmoblie.com.br.medko.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

/**
 * Created by augustuscosta on 22/06/16.
 */
public class RestUtil {
//    52.67.139.39
//    https://medko.herokuapp.com/
//    http://medko.com.br/

    public static final String ROOT_URL_INDEX = "https://medko.herokuapp.com/";
    public static final String ROOT_URL = "https://medko.herokuapp.com/api/";
    public static final String MOBILE_COOKIE = "_medko_session";

    public static void loggerForError(RestClientException e){
        Log.e("ERRO NA CONEXÃO", "Erro ---> MENSAGEM: " + e.getMessage());
        Log.e("ERRO NA CONEXÃO", "Erro ---> CAUSA: " + e.getCause());
        Log.e("ERRO NA CONEXÃO", "Erro ---> MENSAGEM ESPECIFICA: " + e.getMostSpecificCause().getMessage());
    }

    public static void checkUnauthorized(RestClientException e, Context context){
        e.printStackTrace();
        HttpClientErrorException exception = (HttpClientErrorException) e;
        if(HttpStatus.UNAUTHORIZED.equals(exception.getStatusCode())){
            String action = "otgmobile.com.br.medko.UNAUTHORIZED";
            Intent intent = new Intent();
            intent.setAction(action);
            context.sendBroadcast(intent);
        }
    }
}
