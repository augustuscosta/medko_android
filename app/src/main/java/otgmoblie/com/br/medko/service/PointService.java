package otgmoblie.com.br.medko.service;

import android.content.Context;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import otgmoblie.com.br.medko.cloud.PointRest;
import otgmoblie.com.br.medko.model.Point;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by Thialyson on 26/08/16.
 */
@EBean
public class PointService {

    @RootContext
    Context context;

    @RestService
    PointRest pointRest;

    @Bean
    CookieService cookieService;


    public void sendLocation(Point point){

        try{
            pointRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            pointRest.sendLocation(point);
            cookieService.storedCookie(pointRest.getCookie(RestUtil.MOBILE_COOKIE));


        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
        }
    }
}
