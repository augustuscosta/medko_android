package otgmoblie.com.br.medko.views.login;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.List;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Session;
import otgmoblie.com.br.medko.service.DeviceIdService;
import otgmoblie.com.br.medko.service.SessionService;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.util.ValidateUtil;
import otgmoblie.com.br.medko.views.home.HomeActivity_;
import otgmoblie.com.br.medko.views.register.RegisterActivity_;


/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_login_doctor)
public class LoginDoctorFragment extends Fragment {


    @ViewById(R.id.editTextEmailDoctor)
    EditText email;

    @ViewById(R.id.editTextPasswordDoctor)
    EditText password;

    @Bean
    SessionService sessionService;

    @Bean
    DeviceIdService deviceIdService;

    Session session;

    ProgressDialog progress;

    @Click(R.id.buttonLoginDoctor)
    void loginButton() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        checkedList.add(ValidateUtil.checkInterval(password, 6, 18, getString(R.string.invalid_password)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            login();
        }

    }

    @Background
    void login() {

        startProgress();

        session = new Session();
        session.setEmail(email.getText().toString());
        session.setPassword(password.getText().toString());

        try {
            sessionService.signIn(session);
            deviceIdService.sendDeviceId();
            closeProgress();
            callHomeActivity();

        } catch (ResourceAccessException e) {
            closeProgress();
            e.printStackTrace();
            showToast(getString(R.string.internet_no_connected));

        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
            callToastWrongEmailOrPassword();
        }
    }

    @UiThread
    void startProgress() {
        if (getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if (getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }

    @UiThread
    void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void callToastWrongEmailOrPassword() {
        showToast(getString(R.string.email_or_password_incorrect));
    }

    @Click(R.id.buttonGoToRegisterDoctor)
    void goToRegisterDoctor() {
        callRegisterActivity(1);
    }

    @Click(R.id.textViewForgotPassword)
    void goToRecoverPassword() {
        callRegisterActivity(2);
    }

    @UiThread
    void callRegisterActivity(Integer key) {
        startActivity(new Intent(getActivity(), RegisterActivity_.class).putExtra(getString(R.string.key), key));
        getActivity().finish();
    }

    @UiThread
    void callHomeActivity() {
        HomeActivity_.intent(getActivity()).start();
        getActivity().finish();
    }

}
