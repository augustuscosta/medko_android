package otgmoblie.com.br.medko.service;

import android.content.Context;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import otgmoblie.com.br.medko.cloud.PaymentRequestRest;
import otgmoblie.com.br.medko.model.PaymentRequest;
import otgmoblie.com.br.medko.util.RestUtil;

/**
 * Created by augustuscosta on 22/06/16.
 */
@EBean
public class PaymentRequestService {

    @RootContext
    Context context;

    @RestService
    PaymentRequestRest paymentRequestRest;

    @Bean
    CookieService cookieService;

    public String request(Integer id) throws Exception {

        String toReturn = null;

        try {
            paymentRequestRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            PaymentRequest request = paymentRequestRest.getPaymentRequest(id);
            cookieService.storedCookie(paymentRequestRest.getCookie(RestUtil.MOBILE_COOKIE));
            toReturn = request.getUrl();
        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

}
