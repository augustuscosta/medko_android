package otgmoblie.com.br.medko.views.maps;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.service.AvalaibleService;
import otgmoblie.com.br.medko.tasks.ServiceLocation_;
import otgmoblie.com.br.medko.util.DialogUtil;


/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_maps_doctor)
public class MapsFragmentDoctor extends Fragment implements OnMapReadyCallback {

    @ViewById(R.id.switchAvaliable)
    Switch switchAvaliable;

    @Bean
    AvalaibleService avalaibleService;

    GoogleMap googleMap;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5;


    @AfterViews
    void afterViews() {

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        listenSwitchChange();
    }

    void listenSwitchChange(){
        switchAvaliable.setChecked(avalaibleService.getSharedPreferencesAvailable(getActivity()));
        switchAvaliable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    checkIfGpsIsEnabled();
                } else {
                    changeValuesToFalse();
                    getActivity().stopService(new Intent(getActivity(), ServiceLocation_.class));
                }
            }
        });
    }

    void checkIfGpsIsEnabled(){
        final LocationManager gpsIsEnabled = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!gpsIsEnabled.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            changeValuesToFalse();
        } else {
            checkPermission();
        }
    }

    void checkPermission(){
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            changeValuesToFalse();
        } else {
            googleMap.setMyLocationEnabled(true);
            startService();
        }
    }

    void changeValuesToFalse(){
        avalaibleService.updateSharedPreferencesAvailable(false);
        switchAvaliable.setChecked(false);
    }

    void startService(){
        avalaibleService.updateSharedPreferencesAvailable(true);
        getActivity().startService(new Intent(getActivity(), ServiceLocation_.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startService();
                }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng fortaleza = new LatLng(-3.732762, -38.526829);
        googleMap = map;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fortaleza, 14));

    }

    private void buildAlertMessageNoGps() {
        android.app.AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.please_turn_on_your_gps));
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
            }
        });
        builder.show();

    }

}

