package otgmoblie.com.br.medko.views.attendence;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Attendence;
import otgmoblie.com.br.medko.model.AttendenceNewStatus;
import otgmoblie.com.br.medko.model.Classification;
import otgmoblie.com.br.medko.model.TakeTime;
import otgmoblie.com.br.medko.service.AttendenceService;
import otgmoblie.com.br.medko.service.CurrentUserAccountTypeIdService;
import otgmoblie.com.br.medko.service.PaymentRequestService;
import otgmoblie.com.br.medko.util.DialogUtil;
import otgmoblie.com.br.medko.util.NavigationUtil;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.util.RestUtil;
import otgmoblie.com.br.medko.views.PagSeguro.PagSeguroActivity_;
import otgmoblie.com.br.medko.views.home.HomeActivity_;

@EActivity(R.layout.activity_attendence)
public class AttendenceActivity extends AppCompatActivity {

    @ViewById(R.id.toolBar)
    Toolbar toolbar;

    @ViewById(R.id.imageUserAttendence)
    CircleImageView imageAvatar;

    @ViewById(R.id.textViewNameUserAttendence)
    TextView nameUser;

    @ViewById(R.id.textViewExamAttendence)
    TextView examType;

    @ViewById(R.id.textViewSpecialityAttendence)
    TextView speciality;

    @ViewById(R.id.imageButtonTelephone)
    ImageButton telephone;

    @ViewById(R.id.imageButtonArrow)
    ImageButton arrow;

    @ViewById(R.id.textViewPriceAttendence)
    TextView price;

    @ViewById(R.id.dollarSign)
    TextView dollarSign;

    @ViewById(R.id.textViewLocalAttendence)
    TextView localAttendence;

    @ViewById(R.id.textViewAddressAttendence)
    TextView addressAttendence;

    @ViewById(R.id.textViewDateAttendence)
    TextView date;

    @ViewById(R.id.textViewHourAttendence)
    TextView hour;

    @ViewById(R.id.buttonCancelAttendenceMarked)
    Button buttonCancelAttendenceMarked;

    @ViewById(R.id.buttonFinishAttendence)
    Button buttonFinishAttendence;

    @ViewById(R.id.buttonCancelAttendence)
    Button buttonCancelAttendence;

    @ViewById(R.id.textViewStatusAttendence)
    TextView textStatusAttendence;

    @ViewById(R.id.layoutRatingBar)
    LinearLayout layoutRatingBar;

    @ViewById(R.id.linearLayoutTimefixed)
    LinearLayout linearLayoutTimefixed;

    @ViewById(R.id.linearLayoutConfirmCancel)
    LinearLayout linearLayoutConfirmCancel;

    @ViewById(R.id.linearLayoutConfirmCancelPayment)
    LinearLayout linearLayoutConfirmCancelPayment;

    @ViewById(R.id.ratingBarAttendence)
    RatingBar ratingBarAttendence;

    @ViewById(R.id.ratingBarDoctorAttendence)
    RatingBar ratingBarDoctorAttendence;

    @ViewById(R.id.radioGroup)
    RadioGroup radioGroup;

    @ViewById(R.id.radioButton1)
    RadioButton radioButton1;

    @ViewById(R.id.radioButton2)
    RadioButton radioButton2;

    @ViewById(R.id.radioButton3)
    RadioButton radioButton3;

    @ViewById(R.id.radioButton4)
    RadioButton radioButton4;

    @ViewById(R.id.radioButton5)
    RadioButton radioButton5;

    @ViewById(R.id.radioButton6)
    RadioButton radioButton6;

    @Extra
    Integer attendenceId;

    @Bean
    AttendenceService attendenceService;

    @Bean
    CurrentUserAccountTypeIdService currentUserAccountTypeIdService;

    @Bean
    PaymentRequestService paymentRequestService;

    private Integer accountTypeCurrentUser, indexRadioButton, currentAttendenceStatusId;

    private AttendenceNewStatus attendenceNewStatus = new AttendenceNewStatus();

    private String correctUrlAvatar = null, correctNumberTelephone = null;

    private static final int PERMISSIONS_REQUEST_ACCESS_TELEPHONE = 5;

    private Classification classification = new Classification();

    public final static int PAGSEGURO_REQUEST_CODE = 333;

    private List<TakeTime> takeTimesList;

    private Attendence currentAttendence;

    private ProgressDialog progress;

    private Date dateChosen;

    private Bitmap avatar;


    @AfterViews
    void afterViews() {
        accountTypeCurrentUser = currentUserAccountTypeIdService.getSharedPreferencesCurrentUserId(this);
        setSupportActionBar(toolbar);
        requestAttendence();

    }

    @Background
    public void requestAttendence() {

        startProgress();

        try {
            attendenceService.request(attendenceId);
            closeProgress();
            setCurrentAttendence();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }

    }

    @UiThread
    void setCurrentAttendence() {
        currentAttendence = attendenceService.searchForId(attendenceId);
        currentAttendenceStatusId = currentAttendence.getAttendenceStatus().getId();
        setComponents();
    }


    @UiThread
    void setComponents() {
        if (currentAttendence != null) {
            prepareUrlAvatarByAccountType();
            setPersonalAttributes();
            setCommonAttributes();
            setAddressAttendence();
            getStatusByAccountType();
            checkIfUserHasTelephone();
            checkIfAttendenceHasAddress();
        }
    }


    void prepareUrlAvatarByAccountType() {
        switch (accountTypeCurrentUser) {
            case 1:
                correctUrlAvatar = RestUtil.ROOT_URL_INDEX + currentAttendence.getDoctor().getAvatar();
                break;
            case 3:
                correctUrlAvatar = RestUtil.ROOT_URL_INDEX + currentAttendence.getPatient().getAvatar();
                break;
        }
        setAvatar();
    }

    public void setAvatar() {
        if (currentAttendence != null) {
            if (accountTypeCurrentUser == 1) {
                Picasso.with(this).load(correctUrlAvatar).resize(100, 100).centerCrop().error(R.drawable.ic_doctor).into(imageAvatar);
            } else if (accountTypeCurrentUser == 3) {
                Picasso.with(this).load(correctUrlAvatar).resize(100, 100).centerCrop().error(R.drawable.ic_user).into(imageAvatar);
            }
        }
    }

    @UiThread
    void setCommonAttributes() {
        if (currentAttendence != null) {
            setStatusName();
            setDate();
            setValue();
            getStatusByAccountType();
            setExamTypeName();
            setSpecialityName();
        }
    }

    @UiThread
    void setExamTypeName() {
        if (currentAttendence.getExamType() != null) {
            examType.setVisibility(View.VISIBLE);
            examType.setText(currentAttendence.getExamType().getName());
        } else {
            examType.setVisibility(View.GONE);
        }
    }

    @UiThread
    void setSpecialityName() {
        if (currentAttendence.getSpeciality() != null) {
            speciality.setVisibility(View.VISIBLE);
            speciality.setText(currentAttendence.getSpeciality().getName());
        } else {
            speciality.setVisibility(View.GONE);
        }
    }

    @UiThread
    void setStatusName() {
        if (currentAttendence != null) {
            textStatusAttendence.setText(currentAttendence.getAttendenceStatus().getName());
        }
    }

    @SuppressLint("DefaultLocale")
    @UiThread
    void setValue() {

        if (currentAttendence.getHealthPlan() != null) {
            dollarSign.setVisibility(View.GONE);
            price.setText(currentAttendence.getHealthPlan().getName());

        } else {
            if (currentAttendence.getValue() != null) {
                dollarSign.setVisibility(View.VISIBLE);
                price.setText(String.format("%.2f", currentAttendence.getValue()).replace(".", ","));
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    @UiThread
    void setDate() {
        if (currentAttendence.getDate() != null) {
            linearLayoutTimefixed.setVisibility(View.VISIBLE);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            date.setText(dateFormat.format(currentAttendence.getDate()));
            hour.setText(dateFormat.format(currentAttendence.getDate()));

        } else if (currentAttendence.getAttendenceType().getId() == 2 && accountTypeCurrentUser == 1) {
            showRadioGroup();
            listenClickRadioGroup();

        } else if (currentAttendence.getDate() == null) {
            linearLayoutTimefixed.setVisibility(View.VISIBLE);
            date.setText(R.string.not_confirmed);
        }
    }

    @UiThread
    void listenClickRadioGroup() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                int radioButtonID = radioGroup.getCheckedRadioButtonId();
                View radioButton = radioGroup.findViewById(radioButtonID);
                indexRadioButton = radioGroup.indexOfChild(radioButton);

                dateChosen = takeTimesList.get(indexRadioButton).getDate();
            }
        });
    }

    @SuppressLint("SimpleDateFormat")
    @UiThread
    void showRadioGroup() {
        if (currentAttendence.getTakeTimes() != null) {
            radioGroup.setVisibility(View.VISIBLE);
            takeTimesList = new ArrayList<>(currentAttendence.getTakeTimes());

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            setRadioButtons(dateFormat);

        }
    }

    @UiThread
    void setRadioButtons(DateFormat dateFormat) {
        if (takeTimesList != null) {
            radioButton1.setText(dateFormat.format(takeTimesList.get(0).getDate()));
            radioButton2.setText(dateFormat.format(takeTimesList.get(1).getDate()));
            radioButton3.setText(dateFormat.format(takeTimesList.get(2).getDate()));
            radioButton4.setText(dateFormat.format(takeTimesList.get(3).getDate()));
            radioButton5.setText(dateFormat.format(takeTimesList.get(4).getDate()));
            radioButton6.setText(dateFormat.format(takeTimesList.get(5).getDate()));
        }
    }

    @UiThread
    void setPersonalAttributes() {
        switch (accountTypeCurrentUser) {
            case 1:
                nameUser.setText(currentAttendence.getDoctor().getName());
                correctNumberTelephone = currentAttendence.getDoctor().getTelephone();
                showRatingBarDoctor();
                break;
            case 3:
                nameUser.setText(currentAttendence.getPatient().getName());
                correctNumberTelephone = currentAttendence.getPatient().getTelephone();
                break;
        }
        nameUser.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    @UiThread
    void showRatingBarDoctor() {
        if (currentAttendence.getDoctor().getRating() != null && currentAttendence.getDoctor().getRating() && currentAttendence.getDoctor().getMedia() > 0) {
            ratingBarDoctorAttendence.setVisibility(View.VISIBLE);
            ratingBarDoctorAttendence.setRating(currentAttendence.getDoctor().getMedia());
        } else {
            ratingBarDoctorAttendence.setVisibility(View.GONE);
        }
    }


    @UiThread
    void setAddressAttendence() {
        if (currentAttendence.getSearchAddress() != null) {
            localAttendence.setText(getString(R.string.residence));
            addressAttendence.setText(currentAttendence.getSearchAddress().getAddress());
        } else {
            localAttendence.setText(currentAttendence.getPlace().getName());
            addressAttendence.setText(currentAttendence.getPlace().getAddress());
        }
    }

    @UiThread
    void getStatusByAccountType() {

        if (accountTypeCurrentUser != null && accountTypeCurrentUser == 1) {
            switch (currentAttendenceStatusId) {
                case 1:
                    checkIfIsBeforeOrAfterAttendence();
                    break;
                case 2:
                    checkIfAttendenceHasClassification();
                    break;
                case 3:
                    setVisibilityComponents(View.VISIBLE, R.color.tab_background_selected);
                    break;
                case 4:
                    linearLayoutConfirmCancelPayment.setVisibility(View.VISIBLE);
                    break;
                case 5:
                    setVisibilityComponents(View.VISIBLE, R.color.tab_background_green);
                    break;
                case 6:
                    setVisibilityComponents(View.VISIBLE, R.color.background_grey);
                    break;
                case 7:
                    showComponentsByAttendenceTypeId();
                    break;
            }

        } else if (accountTypeCurrentUser != null && accountTypeCurrentUser == 3) {
            switch (currentAttendenceStatusId) {
                case 1:
                    checkIfIsBeforeOrAfterAttendence();
                    break;
                case 2:
                    setVisibilityComponents(View.VISIBLE, R.color.tab_background_green);
                    break;
                case 3:
                    setVisibilityComponents(View.VISIBLE, R.color.tab_background_selected);
                    break;
                case 4:
                    setVisibilityComponents(View.VISIBLE, R.color.background_grey);
                    break;
                case 5:
                    setVisibilityComponents(View.VISIBLE, R.color.tab_background_green);
                    break;
                case 6:
                    linearLayoutConfirmCancel.setVisibility(View.VISIBLE);
                    break;
                case 7:
                    setVisibilityComponents(View.VISIBLE, R.color.background_grey);
                    break;
            }
        }
    }

    @UiThread
    void showComponentsByAttendenceTypeId() {
        if (currentAttendence.getAttendenceType().getId() == 1) {
            linearLayoutConfirmCancelPayment.setVisibility(View.VISIBLE);

        } else if (currentAttendence.getAttendenceType().getId() == 2) {
            radioGroup.setVisibility(View.VISIBLE);
            linearLayoutConfirmCancel.setVisibility(View.VISIBLE);
        }
    }

    @UiThread
    void checkIfAttendenceHasClassification() {
        if (currentAttendence.getClassification() != null) {
            setVisibilityComponents(View.VISIBLE, R.color.tab_background_green);

        } else {
            layoutRatingBar.setVisibility(View.VISIBLE);
            ratingBarAttendence.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                    sendClassification((int) v);
                }
            });
        }
    }

    @Background
    void sendClassification(Integer value) {
        startProgress();
        if (currentAttendence != null) {
            classification.setAttendenceId(currentAttendence.getId());
            classification.setValue(value);
            attendenceService.setClassification(classification);
            closeProgress();
            hideComponents();
            refresh();
        }
        closeProgress();
    }

    @UiThread
    void checkIfIsBeforeOrAfterAttendence() {
        if (currentAttendence.getDate() != null) {
            if (new Date().after(currentAttendence.getDate())) {
                buttonFinishAttendence.setVisibility(View.VISIBLE);
            } else {
                buttonCancelAttendenceMarked.setVisibility(View.VISIBLE);
            }
        }
    }

    @UiThread
    void setVisibilityComponents(Integer visibility, Integer resId) {
        textStatusAttendence.setVisibility(visibility);
        textStatusAttendence.setTextColor(ContextCompat.getColor(this, resId));
    }

    @Click(R.id.imageButtonTelephone)
    void callDialScreen() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_ACCESS_TELEPHONE);

        } else {
            if (correctNumberTelephone != null) {
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + correctNumberTelephone)));
            }
        }
    }

    @Click(R.id.imageButtonArrow)
    void callWazeOrGoogleNavigation() {
        openWazeOrGoogleNavigation();
    }

    @Background
    void openWazeOrGoogleNavigation() {
        if (accountTypeCurrentUser == 1) {

            if (currentAttendence.getPlace() != null) {
                NavigationUtil.openNavigationFromStopMap(this,
                        currentAttendence.getPlace().getLatitude(),
                        currentAttendence.getPlace().getLongitude()
                );
            }
        } else if (accountTypeCurrentUser == 3) {
            if (currentAttendence.getSearchAddress() != null) {
                NavigationUtil.openNavigationFromStopMap(this,
                        currentAttendence.getSearchAddress().getLatitude(),
                        currentAttendence.getSearchAddress().getLongitude()
                );
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    void changeParametersNewStatusAttendence(Integer id, Date date) {
        attendenceNewStatus.setId(id);
        if (date != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm z");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            attendenceNewStatus.setDate(dateFormat.format(date));
        }
    }

    @UiThread
    void hideComponents() {
        buttonCancelAttendenceMarked.setVisibility(View.GONE);
        linearLayoutConfirmCancel.setVisibility(View.GONE);
        buttonFinishAttendence.setVisibility(View.GONE);
        layoutRatingBar.setVisibility(View.GONE);
        radioGroup.setVisibility(View.GONE);
    }


    @Click(R.id.buttonFinishAttendence)
    void finishAttendence() {
        changeStatusToRealized();
    }

    @Background
    void changeStatusToRealized() {
        startProgress();
        changeParametersNewStatusAttendence(currentAttendence.getId(), null);
        if (attendenceService.setAttendenceStatusToRealized(attendenceNewStatus)) {
            closeProgress();
            hideComponents();
            refresh();
        }
        closeProgress();
    }

    @Click(R.id.buttonCancelAttendenceMarked)
    void changeStatusToCancel() {
        changeStatusToCanceled();
    }

    @Background
    void changeStatusToCanceled() {
        startProgress();
        changeParametersNewStatusAttendence(currentAttendence.getId(), null);
        if (attendenceService.setAttendenceStatusToCanceled(attendenceNewStatus)) {
            if (accountTypeCurrentUser == 1) {
                closeProgress();
                callDialogWarningOnCancelAttendence();
            } else {
                closeProgress();
                hideComponents();
                refresh();
            }
        }
        closeProgress();
    }

    @UiThread
    void callDialogWarningOnCancelAttendence() {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(R.string.on_cancel_we_can_block_your_account))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        hideComponents();
                        refresh();
                    }
                });
        builder.show();
    }

    @Click({R.id.buttonCancelAttendence, R.id.buttonCancelAttendencePayment})
    void backToList() {
        onBackPressed();
    }

    @Click(R.id.buttonConfirmAttendence)
    void confirmationAttendence() {
        confirmAttendence();
    }

    @Background
    void confirmAttendence() {
        if (accountTypeCurrentUser == 1) {
            startProgress();

            if (currentAttendence != null && dateChosen != null) {
                changeParametersNewStatusAttendence(currentAttendence.getId(), dateChosen);
                try {
                    attendenceService.setAttendenceStatusWithTime(attendenceNewStatus);
                    closeProgress();
                    hideComponents();
                    refresh();
                } catch (Exception e) {
                    refresh();
                    Toast.makeText(this, "Horário indisponível. Escolha outro horário!", Toast.LENGTH_SHORT).show();
                }
            }

            closeProgress();
        } else {
            startProgress();
            changeParametersNewStatusAttendence(currentAttendence.getId(), null);
            if (attendenceService.setAttendenceStatusToRealized(attendenceNewStatus)) {
                closeProgress();
                hideComponents();
                refresh();
            }
            closeProgress();
        }
    }

    @Click(R.id.buttonConfirmAttendencePayment)
    void callPagSeguroActivity() {
        if (currentAttendence != null) {
            getPaymentRequest(currentAttendence.getId());
        }
    }

    @Background
    void getPaymentRequest(Integer id) {
        startProgress();
        try {
            String url = paymentRequestService.request(id);
            if (url != null) {
                closeProgress();
                callPagSeguroActivity(url);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        closeProgress();
    }

    void callPagSeguroActivity(String url) {
        Intent intent = PagSeguroActivity_.intent(this).url(url).get();
        startActivityForResult(intent, PAGSEGURO_REQUEST_CODE);
    }

    @OnActivityResult(PAGSEGURO_REQUEST_CODE)
    void onResult(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {

            changeParametersNewStatusAttendence(currentAttendence.getId(), null);
            if (attendenceService.setAttendenceStatusToMarked(attendenceNewStatus)) {
                hideComponents();
                refresh();
            }
        }
    }

    @SuppressLint("NewApi")
    @UiThread
    void checkIfUserHasTelephone() {
        if (accountTypeCurrentUser == 1) {
            if (currentAttendence.getDoctor().getTelephone() != null) {
                telephone.setBackground(getDrawable(R.drawable.ic_phone_green));
                telephone.setClickable(true);
            }
        }
        if (accountTypeCurrentUser == 3) {
            if (currentAttendence.getPatient().getTelephone() != null) {
                telephone.setBackground(getDrawable(R.drawable.ic_phone_green));
                telephone.setClickable(true);
            }
        }
    }

    @SuppressLint("NewApi")
    @UiThread
    void checkIfAttendenceHasAddress() {
        if (currentAttendence.getPlace() != null || currentAttendence.getSearchAddress() != null) {
            arrow.setClickable(true);
            arrow.setBackground(getDrawable(R.drawable.ic_navigation_green));
        }
    }

    @UiThread
    void refresh() {
        requestAttendence();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (accountTypeCurrentUser == 1) {
            HomeActivity_.intent(this).start();
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }
}

