package otgmoblie.com.br.medko.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;


/**
 * Created by augustuscosta on 04/07/16.
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class User{

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty("value_home_care")
    private String valueHomeCare;

    @DatabaseField
    @JsonProperty
    private String email;

    @DatabaseField
    @JsonProperty
    private String password;

    @DatabaseField
    @JsonProperty("password_confirmation")
    private String passwordConfirmation;

    @DatabaseField
    @JsonProperty("current_user")
    private Boolean currentUser;

    @DatabaseField
    @JsonProperty
    private String avatar;

    @DatabaseField
    @JsonProperty
    private String telephone;

    @DatabaseField
    @JsonProperty
    private Boolean rating;

    @DatabaseField
    @JsonProperty
    private String crm;

    @DatabaseField
    @JsonProperty("crm_image")
    private String crmImage;

    @DatabaseField
    @JsonProperty("proof_of_addes_image")
    private String proofOfAddesImage;

    @DatabaseField
    @JsonProperty("zip_code")
    private String zipCode;

    @DatabaseField
    @JsonProperty
    private String address;

    @DatabaseField
    @JsonProperty("facebook_account")
    private Boolean facebookAccount;

    @DatabaseField
    @JsonProperty("facebook_token")
    private String facebookToken;

    @DatabaseField
    @JsonProperty("account_type_id")
    private Integer accountTypeId;

    @ForeignCollectionField()
    @JsonProperty("bank_account_attributes")
    private Collection<BankAccount> bankAccounts;

    @ForeignCollectionField()
    @JsonProperty("speciality_attributes")
    private Collection<Speciality> specialities;

    @ForeignCollectionField()
    @JsonProperty("exam_type_attributes")
    private Collection<ExamType> examTypes;

    @DatabaseField
    @JsonProperty("active")
    private Boolean active;

    @DatabaseField
    @JsonProperty
    private Integer media;

    @ForeignCollectionField(eager = false, columnName = "doctor_id")
    @JsonProperty("attendences_as_doctor")
    private Collection<Attendence> attendencesAsDoctor;

    @ForeignCollectionField(eager = false, columnName = "patient_id")
    @JsonProperty("attendences_as_patient")
    private Collection<Attendence> attendencesAsPatient;


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) { this.passwordConfirmation = passwordConfirmation; }

    public String getValueHomeCare() { return valueHomeCare; }

    public void setValueHomeCare(String valueHomeCare) { this.valueHomeCare = valueHomeCare; }

    public Boolean getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Boolean currentUser) { this.currentUser = currentUser; }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTelephone() { return telephone; }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Boolean getRating() { return rating; }

    public void setRating(Boolean rating) {
        this.rating = rating;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getCrmImage() { return crmImage; }

    public void setCrmImage(String crmImage) { this.crmImage = crmImage; }

    public String getProofOfAddesImage() {
        return proofOfAddesImage;
    }

    public void setProofOfAddesImage(String proofOfAddesImage) { this.proofOfAddesImage = proofOfAddesImage; }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(Boolean facebookAccount) { this.facebookAccount = facebookAccount; }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public Collection<BankAccount> getBankAccounts() { return bankAccounts; }

    public void setBankAccounts(Collection<BankAccount> bankAccounts) { this.bankAccounts = bankAccounts; }

    public Collection<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(Collection<Speciality> specialities) { this.specialities = specialities; }

    public Collection<ExamType> getExamTypes() {
        return examTypes;
    }

    public void setExamTypes(Collection<ExamType> examTypes) {
        this.examTypes = examTypes;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getMedia() { return media; }

    public void setMedia(Integer media) { this.media = media; }

    public Collection<Attendence> getAttendencesAsDoctor() {
        return attendencesAsDoctor;
    }

    public void setAttendencesAsDoctor(Collection<Attendence> attendencesAsDoctor) { this.attendencesAsDoctor = attendencesAsDoctor; }

    public Collection<Attendence> getAttendencesAsPatient() {
        return attendencesAsPatient;
    }

    public void setAttendencesAsPatient(Collection<Attendence> attendencesAsPatient) { this.attendencesAsPatient = attendencesAsPatient; }
}
