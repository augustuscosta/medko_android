package otgmoblie.com.br.medko.tasks;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.androidannotations.annotations.EService;
import org.json.JSONException;
import org.json.JSONObject;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.views.attendence.AttendenceActivity_;

/**
 * Created by augustuscosta on 25/08/16.
 */
@EService
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            JSONObject data = new JSONObject(remoteMessage.getData());
            try {
                String notificationType = data.getString("notification_type");
                if("ATTENDENCE_STATUS_CHANGE".equals(notificationType)){
                    String message = data.getString("message");
                    Integer attendenceId = data.getInt("attendence_id");
                    sendAttendenceChangeNotification(message, attendenceId);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void sendAttendenceChangeNotification(String messageBody, Integer attendenceId) {
        Intent intent = AttendenceActivity_.intent(this).attendenceId(attendenceId).get();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}