package otgmoblie.com.br.medko.views.register;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mvc.imagepicker.ImagePicker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.client.ResourceAccessException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import otgmoblie.com.br.medko.R;
import otgmoblie.com.br.medko.model.Address;
import otgmoblie.com.br.medko.model.BankAccount;
import otgmoblie.com.br.medko.model.ExamType;
import otgmoblie.com.br.medko.model.Speciality;
import otgmoblie.com.br.medko.model.User;
import otgmoblie.com.br.medko.service.DeviceIdService;
import otgmoblie.com.br.medko.service.ExamTypeService;
import otgmoblie.com.br.medko.service.RegistrationService;
import otgmoblie.com.br.medko.service.SpecialityService;
import otgmoblie.com.br.medko.service.UserService;
import otgmoblie.com.br.medko.util.DialogUtil;
import otgmoblie.com.br.medko.util.ProgressDialogUtil;
import otgmoblie.com.br.medko.util.ValidateUtil;
import otgmoblie.com.br.medko.views.home.HomeActivity_;
import otgmoblie.com.br.medko.views.login.LoginActivity_;


@EFragment(R.layout.fragment_register_doctor)
public class RegisterDoctorFragment extends Fragment {

    @ViewById(R.id.imageViewRegisterUser)
    de.hdodenhof.circleimageview.CircleImageView avatarImage;

    @ViewById(R.id.editTextNameDoctor)
    EditText name;

    @ViewById(R.id.editTextCRMDoctor)
    EditText crm;

    @ViewById(R.id.editTextPhoneDoctor)
    EditText telephone;

    @ViewById(R.id.checkBoxAccept)
    CheckBox acceptClassification;

    @ViewById(R.id.editTextPriceHomeCare)
    EditText valueHomeCare;

    @ViewById(R.id.editTextEmailDoctor)
    EditText email;

    @ViewById(R.id.editTextPasswordDoctor)
    EditText password;

    @ViewById(R.id.editTextConfirmationPasswordDoctor)
    EditText passwordConfirmation;

    @ViewById(R.id.editTextViewCRM)
    EditText crmImagee;

    @ViewById(R.id.editTextViewProofOfAddress)
    EditText proofOfAddressImagee;

    @ViewById(R.id.editTextCEP)
    EditText cep;

    @ViewById(R.id.editTextAddress)
    EditText editTextaddress;

    @ViewById(R.id.editTextFullName)
    EditText fullName;

    @ViewById(R.id.editTextCPFCNPJ)
    EditText cpfCnpj;

    @ViewById(R.id.editTextBank)
    EditText bank;

    @ViewById(R.id.editTextAgency)
    EditText agency;

    @ViewById(R.id.editTextAccount)
    EditText account;

    @ViewById(R.id.editTextOperation)
    EditText operation;

    @ViewById(R.id.textViewTermsAndServices)
    TextView termsAndConditions;

    @Bean
    SpecialityService specialityService;

    @Bean
    ExamTypeService examTypeService;

    @Bean
    UserService userService;

    @Bean
    RegistrationService registrationService;

    @Bean
    DeviceIdService deviceIdService;

    private List<Speciality> specialitiesList = new ArrayList<>();

    private List<ExamType> examTypesList = new ArrayList<>();

    private Bitmap avatarBitmap, crmBitmap, proofOfAddressBitmap;

    private boolean boolCRM = false, boolProofOfAddress = false;

    private int valueImage;

    private ProgressDialog progress;

    private static final int PICK_IMAGE_ID = 234;

    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 5;

    @AfterViews
    void afterVies() {
        requests();
        acceptClassification.setChecked(true);
        listenChangeFocusCEP();
    }

    @Background
    void requests() {
        startProgress();
        try {
            specialityService.request();
            examTypeService.request();
            closeProgress();

        } catch (ResourceAccessException e) {
            closeProgress();
            e.printStackTrace();
            showToast(getString(R.string.internet_no_connected));

        } catch (Exception e) {
            closeProgress();
            closeProgress();
            e.printStackTrace();
        }
    }

    @UiThread
    void listenChangeFocusCEP() {
        cep.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                checkCep(hasFocus);
            }
        });
    }

    @Background
    void checkCep(boolean hasFocus) {
        if (cep != null && !hasFocus) {

            try {
                Address address = userService.getAddressByCep(cep.getText().toString());
                setText(address);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @SuppressLint("SetTextI18n")
    @UiThread
    void setText(Address address) {
        if (address != null && address.getLogradouro() != null) {
            editTextaddress.setText(
                    address.getLogradouro() + ", " +
                            address.getComplemento() + " - " +
                            address.getBairro() + ", " +
                            address.getLocalidade() + " - " +
                            address.getUf());
        }
    }

    @Click(R.id.buttonAvatarImage)
    void takeAPickAvatar(View view) {
        checkPermission(PICK_IMAGE_ID);
        valueImage = 1;
    }

    @Click(R.id.buttonCRMImage)
    void takeAPickCRM(View view) {
        checkPermission(PICK_IMAGE_ID);
        valueImage = 2;
    }

    @Click(R.id.buttonProofOfAddressImage)
    void takeAPickCRMProofOfAddress(View view) {
        checkPermission(PICK_IMAGE_ID);
        valueImage = 3;
    }

    @Click(R.id.buttonRegisterDoctor)
    void registrar() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.checkInterval(name, 1, 45, getString(R.string.invalid_name)));
        checkedList.add(ValidateUtil.checkInterval(crm, 4, 18, getString(R.string.invalid_crm)));
        checkedList.add(ValidateUtil.checkInterval(telephone, 10, 11, getString(R.string.invalid_number)));
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        checkedList.add(ValidateUtil.checkInterval(password, 6, 18, getString(R.string.invalid_password)));
        checkedList.add(ValidateUtil.checkConfirmPassword(password, passwordConfirmation, getString(R.string.diferent_passwords)));
        checkedList.add(ValidateUtil.checkImageView(getString(R.string.photo_crm_required), boolCRM, getActivity()));
        checkedList.add(ValidateUtil.checkImageView(getString(R.string.photo_proof_of_address_required), boolProofOfAddress, getActivity()));
        checkedList.add(ValidateUtil.checkInterval(cep, 8, 8, getString(R.string.invalid_cep)));
        checkedList.add(ValidateUtil.checkInterval(editTextaddress, 1, 80, getString(R.string.invalid_address)));
        checkedList.add(ValidateUtil.checkInterval(fullName, 1, 45, getString(R.string.invalid_name)));
        checkedList.add(ValidateUtil.checkCpfCnpj(cpfCnpj, 11, 14, getString(R.string.invalid_cpf_cnpj)));
        checkedList.add(ValidateUtil.checkInterval(bank, 1, 20, getString(R.string.invalid_bank)));
        checkedList.add(ValidateUtil.checkInterval(agency, 1, 10, getString(R.string.invalid_agency)));
        checkedList.add(ValidateUtil.checkInterval(account, 1, 20, getString(R.string.invalid_account)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {

            getImageAndRegister(avatarBitmap, crmBitmap, proofOfAddressBitmap);
        }

    }

    @Background
    void getImageAndRegister(Bitmap avatarBitmap, Bitmap crmBitmap, Bitmap proofOfAddressBitmap) {

        User user = returnUser(new User());

        FileSystemResource imageCrm = new FileSystemResource(persistImage(crmBitmap, getString(R.string.crm)));
        FileSystemResource imageProofOfAddress = new FileSystemResource(persistImage(proofOfAddressBitmap, getString(R.string.proof_of_address)));

        startProgress();
        if (avatarBitmap != null) {
            FileSystemResource imageAvatar = new FileSystemResource(persistImage(avatarBitmap, getString(R.string.avatar)));

            if (registrationService.register(user, imageAvatar, imageCrm, imageProofOfAddress) != null) {
                closeProgress();
                callHomeActivity();

            } else {
                closeProgress();
                callDialogLoginActivity();
            }

        } else {

            if (registrationService.register(user, null, imageCrm, imageProofOfAddress) != null) {
                closeProgress();
                callHomeActivity();

            } else {
                closeProgress();
                callDialogLoginActivity();
            }
        }
    }


    @UiThread
    void callHomeActivity() {
        deviceIdService.sendDeviceId();
        HomeActivity_.intent(getActivity()).start();
        getActivity().finish();
    }

    @UiThread
    void callDialogLoginActivity() {
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.access_denied_doctor))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        callLoginActivity();
                    }
                });
        builder.show();
    }

    @UiThread
    void callLoginActivity() {
        LoginActivity_.intent(getActivity()).start();
        getActivity().finish();

    }

    private File persistImage(Bitmap bitmap, String name) {
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        try {
            OutputStream os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            return imageFile;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public User returnUser(User user) {
        user.setName(name.getText().toString());
        user.setCrm(crm.getText().toString());
        user.setTelephone(telephone.getText().toString());
        user.setValueHomeCare(valueHomeCare.getText().toString());
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());
        user.setPasswordConfirmation(passwordConfirmation.getText().toString());
        user.setSpecialities(specialitiesList);
        user.setExamTypes(examTypesList);
        user.setZipCode(cep.getText().toString());
        user.setAddress(editTextaddress.getText().toString());
        user.setAccountTypeId(3);
        user.setActive(false);
        if (!acceptClassification.isChecked()) {
            user.setRating(false);
        } else {
            user.setRating(true);
        }
        Collection<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(returnBankAccount());
        user.setBankAccounts(bankAccounts);

        return user;
    }

    public BankAccount returnBankAccount() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setFullName(fullName.getText().toString());
        bankAccount.setCpfCnpj(cpfCnpj.getText().toString());
        bankAccount.setBank(bank.getText().toString());
        bankAccount.setAgency(agency.getText().toString());
        bankAccount.setAccount(account.getText().toString());
        bankAccount.setOperation(operation.getText().toString());
        return bankAccount;
    }

    @Click(R.id.buttonSpecialitiesDoctor)
    void specialitiesButton() {
        showSpecialistDialog();
    }

    @Click(R.id.buttonExamTypesDoctor)
    void examTypesButton() {
        showExamTypeDialog();
    }


    public void showSpecialistDialog() {
        final List<Integer> returnedTrues = new ArrayList<>();
        final CharSequence[] namesItemsList = getStringsArrayFromSpecialities();
        final boolean[] checkedItems = new boolean[namesItemsList.length];

        getCheckedItemsSpecialities(checkedItems);

        AlertDialog.Builder builder = getDialogBuilder();
        showMultiChoiceItems(builder, namesItemsList, checkedItems);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < checkedItems.length; i++) {
                    if (checkedItems[i]) {
                        returnedTrues.add(i);
                    }

                }
                getSelectedSpecialities(returnedTrues);
            }
        });
        showNegativeButton(builder);
        builder.create().show();
    }


    public void showExamTypeDialog() {
        final List<Integer> returnedTrues = new ArrayList<>();
        final CharSequence[] namesItemsList = getStringArrayFromExamTypes();
        final boolean[] checkedItems = new boolean[namesItemsList.length];
        getCheckedItemsExamTypes(checkedItems);

        AlertDialog.Builder builder = getDialogBuilder();
        showMultiChoiceItems(builder, namesItemsList, checkedItems);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < checkedItems.length; i++) {
                    if (checkedItems[i])
                        returnedTrues.add(i);
                }
                getSelectedExamTypes(returnedTrues);
            }
        });
        showNegativeButton(builder);
        builder.create().show();
    }

    @NonNull
    private AlertDialog.Builder getDialogBuilder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.select);
        builder.setCancelable(false);
        return builder;
    }


    private AlertDialog.Builder showMultiChoiceItems(AlertDialog.Builder builder, CharSequence[] namesItemsList, final boolean[] checkedItems) {
        builder.setMultiChoiceItems(namesItemsList, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index, boolean checked) {
                checkedItems[index] = checked;
            }
        });
        return builder;
    }


    private AlertDialog.Builder showNegativeButton(AlertDialog.Builder builder) {
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return builder;
    }


    private CharSequence[] getStringsArrayFromSpecialities() {
        List<Speciality> tempList = specialityService.getAllSpecialitiesByName();
        CharSequence[] namesSpecialities = new CharSequence[tempList.size()];
        for (int i = 0; i < tempList.size(); i++) {
            namesSpecialities[i] = tempList.get(i).getName();
        }
        return namesSpecialities;
    }


    private CharSequence[] getStringArrayFromExamTypes() {
        List<ExamType> tempList = examTypeService.getAllExamTypesByName();
        CharSequence[] namesExamTypes = new CharSequence[tempList.size()];
        for (int i = 0; i < tempList.size(); i++) {
            namesExamTypes[i] = tempList.get(i).getName();
        }
        return namesExamTypes;
    }


    private void getSelectedSpecialities(List<Integer> returnedTrues) {
        for (Integer indice : returnedTrues) {
            specialitiesList.add(specialityService.getAllSpecialitiesByName().get(indice));
        }
    }

    private void getSelectedExamTypes(List<Integer> returnedTrues) {
        for (Integer indice : returnedTrues) {
            examTypesList.add(examTypeService.getAllExamTypesByName().get(indice));
        }
    }


    private void getCheckedItemsSpecialities(boolean[] checkedItems) {
        specialitiesList = new ArrayList<>();
        for (Speciality speciality : specialitiesList) {
            checkedItems[specialityService.getAll().indexOf(speciality)] = true;

        }
    }

    private void getCheckedItemsExamTypes(boolean[] checkedItems) {
        examTypesList = new ArrayList<>();
        for (ExamType examType : examTypesList) {
            checkedItems[examTypeService.getAll().indexOf(examType)] = true;
        }
    }


    @Background
    public void checkPermission(Integer integer) {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
            onPickImage(new View(getActivity()), integer);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onPickImage(new View(getActivity()), requestCode);
                }
                break;
        }
    }

    @Background
    public void onPickImage(View view, Integer integer) {
        try {
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity(), getString(R.string.choose));
            startActivityForResult(chooseImageIntent, integer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = ImagePicker.getImageFromResult(getActivity(), requestCode, resultCode, data);
        switch (valueImage) {
            case 1:
                if (bitmap != null) {
                    avatarBitmap = bitmap;
                    avatarImage.setBackground(null);
                    avatarImage.setImageBitmap(bitmap);
                }
                break;
            case 2:
                if (bitmap != null) {
                    boolCRM = true;
                    crmBitmap = bitmap;
                    crmImagee.setTextColor(ContextCompat.getColor(getActivity(), R.color.tab_background_green));
                }
                break;
            case 3:
                if (bitmap != null) {
                    boolProofOfAddress = true;
                    proofOfAddressBitmap = bitmap;
                    proofOfAddressImagee.setTextColor(ContextCompat.getColor(getActivity(), R.color.tab_background_green));
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @UiThread
    void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void userLogged() {
        showToast(getString(R.string.exists_a_session_already));
    }

    @Click(R.id.textViewTermsAndServices)
    void goToTermsAndServices() {
        termsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @UiThread
    void startProgress() {
        if (getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if (getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }

}